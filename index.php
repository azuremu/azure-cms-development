<?php

define('Start', microtime(true));
/*
 * * Azure CMS
 * * The Azure Project
 * * Maked by Claudio Santoro (bi0s)
 * * fb.com/sant0ros twitter: @m0vame skype: live:sant0ro
 */

/*
 * * Start the Session
 */

session_start();

/*
 * * Settings of CMS
 */

$Settings    = array('host' => 'localhost', 'user' => 'root', 'pass' => 'latitude', 'name' => 'mercury', 'port' => 3306, 'type' => 'mysql');
$Hotel       = array('hotel_name' => 'azure', 'hotel_url' => 'http://localhost/', 'gallery_url' => 'web-gallery/', 'swf_url' => 'http://localhost/', 'client_name' => '?p=client', 'comb_name' => '1.xml', 'use_comb' => TRUE, 'emu_ip' => '127.0.0.1', 'emu_port' => '300', 'safe_emu' => FALSE, 'swf_name' => 'Habbo.swf', 'gordon_path' => 'RELEASE63-201411201226-580134750/', 'fb_appid' => '1417574575138432', 'fb_secret' => '', 'sonic_id' => '2abb40ad', 'fb_int_name' => 'habboen', 'fb_int_id' => '65d5e60e738877cb53bb5004edf6a8fc', 'twitter_on' => 'visible', 'twitter_name' => 'Habbo', 'twitter_id' => '345163210678206465', 'paypalurl' => 'www.paypal.com/order/4535535/');
$comb_values = array('variables' => '1.xml', 'texts' => '1.xml', 'figuredata' => '1.xml', 'furnidata' => '1.xml', 'productdata' => '1.xml', 'o_variables' => '1.xml', 'o_texts' => '1.xml');
$SubHeader   = array('pages' => array('me' => 'Home', 'staff' => 'Staff', 'safety' => 'Safety', 'credits' => 'Credits', 'donate' => 'Donate', 'rares' => 'Rares'), 'subpages' => array('logout' => 'Sign Out'));

/*
 * * Classes
 */

final class Hotel
    {

    var $online_users;
    var $registered_users;
    var $Server_Ver;

    public function __construct($Online_Users, $Server_Ver, $Registered_Users)
        {
        $this->online_users     = $Online_Users;
        $this->registered_users = $Registered_Users;
        $this->Server_Ver       = $Server_Ver;
        }

    public function getHotel($Var)
        {
        return $this->$Var;
        }

    }

final class Habbo
    {

    var $Id;
    var $Name;
    var $Mail;
    var $Gender;
    var $Motto;
    var $Credits;
    var $Duckets;
    var $Ip;
    var $Ticket;
    var $Look;
    var $IsAdmin;

    public function __construct($Id, $Name, $Mail, $Gender, $Motto, $Credits, $Duckets, $Ip, $Ticket, $Look, $IsAdmin)
        {
        $this->Id      = $Id;
        $this->Name    = $Name;
        $this->Mail    = $Mail;
        $this->Gender  = $Gender;
        $this->Motto   = $Motto;
        $this->Credits = $Credits;
        $this->Duckets = $Duckets;
        $this->Ip      = $Ip;
        $this->Ticket  = $Ticket;
        $this->Look    = $Look;
        $this->IsAdmin = $IsAdmin;
        }

    public function getHabbo($Var)
        {
        return $this->$Var;
        }

    public static function Ticket()
        {
        for ($i = 1; $i <= 10; $i++)
            $data = $data . rand(0, 8);
        $data = $data . "d";
        $data = $data . rand(0, 4);
        $data = $data . "c";
        $data = $data . rand(0, 6);
        $data = $data . "c";
        $data = $data . rand(0, 8);
        $data = $data . "c";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 4);
        $data = $data . "d";
        for ($i = 1; $i <= 3; $i++)
            $data = $data . rand(0, 6);
        $data = $data . "ae";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 6);
        $data = $data . "bcb";
        $data = $data . rand(0, 4);
        $data = $data . "a";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 8);
        $data = $data . "c";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 4);
        $data = $data . "a";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 8);
        return $data;
        }

    public static function UserHash()
        {
        for ($i = 1; $i <= 10; $i++)
            $data = $data . rand(0, 9);
        $data = $data . "d";
        $data = $data . rand(0, 7);
        $data = $data . "c";
        $data = $data . rand(0, 5);
        $data = $data . "c";
        $data = $data . rand(0, 3);
        $data = $data . "c";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 5);
        $data = $data . "d";
        for ($i = 1; $i <= 3; $i++)
            $data = $data . rand(0, 7);
        $data = $data . "ae";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 7);
        $data = $data . "bcb";
        $data = $data . rand(0, 5);
        $data = $data . "a";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 9);
        $data = $data . "c";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 3);
        $data = $data . "a";
        for ($i = 1; $i <= 2; $i++)
            $data = $data . rand(0, 9);
        return $data;
        }

    public function WName()
        {
        for ($i = 1; $i <= 30; $i++)
            $data = $data . rand(0, 9);
        return $data;
        }

    }

final
        class Database
    {

    private
            function __construct()
        {
        
        }

    public static function Open($db)
        {

        if (isset($db))
            {
            $user = isset($db['user']) ? $db['user'] : NULL;
            $pass = isset($db['pass']) ? $db['pass'] : NULL;
            $name = isset($db['name']) ? $db['name'] : NULL;
            $host = isset($db['host']) ? $db['host'] : NULL;
            $type = isset($db['type']) ? $db['type'] : NULL;
            $port = isset($db['port']) ? $db['port'] : NULL;

            switch ($type)
                {
                case 'pgsql':
                    $port = $port ? $port : '5432';
                    $Conn = new PDO("pgsql:dbname={$name}; user={$user}; password={$pass};
                        host=$host;port={$port}");
                    break;
                case 'mysql':
                    $port = $port ? $port : '3306';
                    if ($pass != NULL)
                        $Conn = new PDO("mysql:host={$host};port={$port};dbname={$name}", $user, $pass);
                    else
                        $Conn = new PDO("mysql:host={$host};port={$port};dbname={$name}", $user);
                    break;
                case 'sqlite':
                    $Conn = new PDO("sqlite:{$name}");
                    break;
                case 'ibase':
                    $Conn = new PDO("firebird:dbname={$name}", $user, $pass);
                    break;
                case 'oci8':
                    $Conn = new PDO("oci:dbname={$name}", $user, $pass);
                    break;
                case 'mssql':
                    $Conn = new PDO("mssql:host={$host},1433;dbname={$name}", $user, $pass);
                    break;
                }
            }

        $Conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $Conn;
        }

    }

final class Adapter
    {

    private static $Conn;

    private function __construct()
        {
        
        }

    public static function Open($database)
        {
        if (empty(self::$Conn)):
            self::$Conn = Database::Open($database);
            return true;
        endif;
        }

    public static function Get()
        {
        return self::$Conn;
        }

    public static function Close()
        {
        if (self::$Conn):
            self::$Conn = NULL;
        endif;
        }

    public static function Query($Query = NULL)
        {
        try
            {
            if (isset(self::$Conn)):
                $Result = self::$Conn->query($Query);
                if (isset($Result)):
                    return $Result;
                endif;
            else:
                echo xEcho("Exception on Violation Database: Database not Started");
                return "not_started";
            endif;
            }
        catch (PDOException $Ex)
            {
            echo xEcho("Exception on Violation Database: " . $Ex);
            }
        }

    public static function FetchAll($Result = NULL)
        {

        try
            {
            if (isset($Result)):
                if ($Result != "not_started"):
                    $row = $Result->fetch(PDO::FETCH_ASSOC);
                    return $row;
                else:
                    echo xEcho("Exception on Violation Database: Database not Started");
                endif;
            else:
                return "";
            endif;
            }
        catch (PDOException $Ex)
            {
            echo xEcho("Exception on Violation Database: " . $Ex);
            }
        }

    public static function InsertArray($table, $data, $exclude = array())
        {
        $fields  = $values  = array();
        if (!is_array($exclude))
            $exclude = array($exclude);
        foreach (array_keys($data) as $key)
            {
            if (!in_array($key, $exclude)):
                $fields[] = "`$key`";
                $values[] = "'" . htmlentities(addslashes($data[$key])) . "'";
            endif;
            }
        $fields = implode(",", $fields);
        $values = implode(",", $values);
        Adapter::Query("INSERT INTO `$table` ($fields) VALUES ($values)");
        }

    public static function Counts($Query = NULL)
        {
        try
            {
            if (isset($Query)):
                if ($Query != "not_started"):
                    $Values = $Query->rowCount();
                    return $Values;
                else:
                    echo xEcho("Exception on Violation Database: Database not Started");
                endif;
            else:
                return "";
            endif;
            }
        catch (PDOException $Ex)
            {
            echo xEcho("Exception on Violation Database: " . $Ex);
            }
        }

    }

class Script
    {

    var $Scripts;

    public function __construct($Url)
        {
        $this->Scripts = "<script src='$Url' type='text/javascript'></script>" . "\n\r";
        }

    public function Add($Url)
        {
        $this->Scripts .= "<script src='$Url' type='text/javascript'></script>" . "\n\r";
        }

    public function Get()
        {
        return $this->Scripts;
        }

    }

class ScriptCode
    {

    var $Scripts;

    public function __construct()
        {
        $this->Scripts = "<script type='text/javascript'>" . "\n\r";
        }

    public function Add($Code)
        {
        $this->Scripts .= $Code;
        }

    public function Get()
        {
        $this->Scripts .= "</script>";
        return $this->Scripts;
        }

    }

class Stylesheet
    {

    var $Stylesheets;

    public function __construct($Url)
        {
        $this->Stylesheets = "<link rel='stylesheet' href='$Url' type='text/css'/>" . "\n\r";
        }

    public function Add($Url)
        {
        $this->Stylesheets .= "<link rel='stylesheet' href='$Url' type='text/css'/>" . "\n\r";
        }

    public function Get()
        {
        return $this->Stylesheets;
        }

    }

class Header
    {

    var $Scripts;
    var $Styles;
    var $Code;

    public function __construct($WebGallery, $Page = 'default')
        {
        $this->Scripts = new Script("");
        $this->Styles  = new Stylesheet("");
        switch ($Page)
            {
            case 'client':
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/common.js");
                $this->Styles->Add($WebGallery . "static/styles/habboflashclient.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                break;
            case 'fproblem':
                $this->Styles->Add($WebGallery . "v2/styles/process.css");
                $this->Styles->Add($WebGallery . "static/styles/habboflashclient.css");
                $this->Styles->Add($WebGallery . "static/styles/v3_habblet.css");
                break;
            case 'me':
                $this->Scripts->Add($WebGallery . "static/js/lightweightmepage.js");
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                $this->Styles->Add($WebGallery . "static/styles/lightweightmepage.css");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                break;
            case 'credits':
            case 'rares':
                $this->Scripts->Add($WebGallery . "static/js/lightweightmepage.js");
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                $this->Styles->Add($WebGallery . "static/styles/lightweightmepage.css");
                $this->Styles->Add($WebGallery . "static/styles/newcredits.css");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                break;
            case 'staff':
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                break;
            case 'index':
                $this->Scripts->Add($WebGallery . "static/js/libs2.js");
                $this->Styles->Add($WebGallery . "v2/styles/process.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                break;
            case 'register':
                $this->Scripts->Add($WebGallery . "static/js/libs2.js");
                $this->Styles->Add($WebGallery . "v2/styles/process.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/buttons.css");
                break;
            case 'createnews':
                $this->Styles->Add($WebGallery . "static/styles/lightweightmepage.css");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/process.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                $this->Styles->Add($WebGallery . "v2/styles/buttons.css");
                break;
            case 'safety':
                $this->Styles->Add($WebGallery . "static/styles/safety.css");
                $this->Styles->Add($WebGallery . "v2/styles/buttons.css");
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                break;
            case 'welcome':
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/process.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                $this->Styles->Add($WebGallery . "v2/styles/buttons.css");
                $this->Styles->Add($WebGallery . "v2/styles/welcome.css");
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                break;
            default:
                $this->Styles->Add($WebGallery . "static/styles/common.css");
                $this->Styles->Add($WebGallery . "v2/styles/process.css");
                $this->Styles->Add($WebGallery . "v2/styles/style.css");
                $this->Styles->Add($WebGallery . "v2/styles/buttons.css");
                $this->Scripts->Add($WebGallery . "static/js/visual.js");
                $this->Scripts->Add($WebGallery . "static/js/libs.js");
                break;
            }
        return;
        }

    public function Get()
        {
        $C = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n";
        $C .= "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
        $C .= "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
        $C .= "<head>\n";
        $C .= "<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\" />\n";
        $C .= "<title>Habbo </title>\n";
        $C .= "<meta name=\"description\" content=\"Habbo is a virtual world where you can meet and make friends.\" />\n";
        $C .= "<meta name=\"keywords\" content=\"Habbo,Habbo,virtual world,play games,enter competitions,make friends\" />\n";
        $C .= $this->Scripts->Get();
        $C .= $this->Styles->Get();
        $C .= "</head>\n";
        return $C;
        }

    }

class Tags
    {

    private $name;          // nome da TAG
    private $properties;    // propriedades da TAG
    protected $children;

    public function __construct($name)
        {

        $this->name = $name;
        }

    public function __set($name, $value)
        {

        $this->properties[$name] = $value;
        }

    public function add($child)
        {
        $this->children[] = $child;
        }

    private function open()
        {
        $Code = '';
        $Code .= "<{$this->name}";
        if ($this->properties)
            {
            foreach ($this->properties as $name => $value)
                {
                $Code .= " {$name}=\"{$value}\"";
                }
            }
        $Code .= '>';
        return $Code;
        }

    public function show()
        {
        $Code = '';
        $Code .= $this->open();
        $Code .= "\n";

        if ($this->children)
            {

            foreach ($this->children as $child)
                {

                if (is_object($child))
                    {
                    $Code .= $child->show();
                    }
                else if ((is_string($child)) or ( is_numeric($child)))
                    {

                    $Code .= $child;
                    }
                }

            $Code .= $this->close();
            }
        return $Code;
        }

    private function close()
        {
        $Code = '';
        $Code .= "</{$this->name}>\n";
        return $Code;
        }

    }

class Content
    {

    var $Body;
    var $Content;
    var $Container;

    public function StartBody($PageId)
        {
        switch ($PageId)
            {
            case 'me':
            case 'welcome':
            case 'staff':
            case 'safety':
                $PageIdType  = 'home';
                $Style       = 'width: 100%;padding: 0;background: #eee !important;';
                $PageIdClass = ' ';
                break;
            case 'credits':
                $PageIdType  = 'newcredits';
                $Style       = ' ';
                $PageIdClass = ' ';
                break;
            case 'fproblem':
                $PageIdType  = 'popup';
                $PageIdClass = 'process-template client_error';
                $Style       = ' ';
                break;
            case 'articles':
                $PageIdType  = 'news';
                $PageIdClass = ' ';
                $Style       = ' ';
                break;
            case 'index':
            case 'register':
            case 'createnews':
                $PageIdType  = ' ';
                $PageIdClass = 'process-template';
                $Style       = 'width: 100%;padding:0;background-color:rgb(241, 241, 241);color:rgb(50, 50, 50);';
                break;
            case 'client':
                $PageIdType  = 'client';
                $PageIdClass = ' ';
                $Style       = ' ';
                break;
            default:
                $PageIdType  = 'home';
                $PageIdClass = ' ';
                $Style       = ' ';
                break;
            }
        switch ($PageId)
            {
            case 'register':
            case 'createnews':
            case 'index':
                $Body            = new Tags('body');
                $Body->id        = $PageIdType;
                $Body->class     = $PageIdClass;
                $Body->style     = $Style;
                $this->Body      = $Body;
                $Container       = new Tags('div');
                $Container->id   = 'container';
                $this->Body      = $Body;
                $this->Container = $Container;
                break;
            case 'me':
            case 'welcome':
            case 'staff':
            case 'credits':
            case 'rares':
            case 'donate':
            case 'safety':
            case 'articles':
                $Body            = new Tags('body');
                $Body->id        = $PageIdType;
                $Body->class     = $PageIdClass;
                $Body->style     = $Style;
                $Code            = '';
                $Code .= "<link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>\n";
                $Code .= "<div id=\"header-container\" style=\"background: url(http://foundation.zurb.com/assets/img/marquee-stars.svg) #074e68;height: 110px;\">\n";
                $Code .= "<div id=\"header\" class=\"clearfix\" style=\"background:none;\">\n";
                $Code .= Page::LogoHeader();
                $Code .= Page::SubHeader($PageId);
                $Body->add($Code);
                $Container       = new Tags('div');
                $Container->id   = 'container';
                $Content         = new Tags('div');
                $Content->id     = 'content';
                $this->Body      = $Body;
                $this->Container = $Container;
                $this->Content   = $Content;
                break;
            case 'client':
                $this->Body      = '';
                break;
            default:
                $Body            = new Tags('body');
                $Body->id        = $PageIdType;
                $Body->class     = $PageIdClass;
                $Body->style     = $Style;
                $Container       = new Tags('div');
                $Container->id   = 'container';
                $Content         = new Tags('div');
                $Content->id     = 'content';
                $this->Body      = $Body;
                $this->Container = $Container;
                $this->Content   = $Content;
                break;
            }
        }

    public function AddContent($Code)
        {
        $this->Body->add($Code);
        }

    public function EndBody($Footer = 1)
        {
        $Code = $this->Body->show();
        return $Code;
        }

    public function GenericBody($Code = '')
        {
        if (!empty($this->Container) && !empty($this->Content)):
            $this->Content->add($Code);
            $this->Container->add($this->Content);
            $this->Body->add($this->Container);
        elseif (empty($this->Container) && !empty($this->Content)):
            $this->Content->add($Code);
            $this->Body->add($this->Content);
        elseif (!empty($this->Container) && empty($this->Content)):
            $this->Container->add($Code);
            $this->Body->add($this->Container);
        endif;
        if (!empty($this->Body)):
            $Code = $this->Body->show();
        endif;
        return $Code;
        }

    public static function HabbleContainer($Type, $Content, $Title = '')
        {
        $Tag         = new Tags('div');
        $Tag->class  = 'habblet-container';
        $Tag2        = new Tags('div');
        $Tag2->class = "cbb clearfix $Type";
        $Tag->add($Tag2);
        if (!empty($Title)):
            $Tag3        = new Tags('h2');
            $Tag3->class = 'title';
            $Tag3->add($Title);
            $Tag2->add($Tag3);
        else:
            $Tag2->add('');
        endif;
        $Tag4        = new Tags('div');
        $Tag4->class = 'box-content';
        $Tag4->add($Content);
        $Tag2->add($Tag4);
        return $Tag;
        }

    }

class Body
    {

    public static function Overlay($Enabled = 0)
        {
        $Tag     = new Tags('div');
        $Tag->id = 'overlay';
        if ($Enabled):
            $Tag->style = 'display:block;height:100%;z-index:9000;position:fixed;';
        endif;
        $Tag->add('');
        $Tag = $Tag->show();
        return $Tag;
        }

    public static function Page($PageId)
        {
        if (!isset($Code))
            $Code = "";
        $Body = new Content;
        $Body->StartBody($PageId);
        switch ($PageId)
            {
            case "fproblem":
                if (!isset($Code))
                    $Code         = "";
                $Tag1         = new Tags('div');
                $Tag1->id     = 'process-content';
                $Tag1->class  = 'centered-client-error';
                $Tag2         = new Tags('div');
                $Tag2->id     = 'column1';
                $Tag2->class  = 'column';
                $Tag1->add($Tag2);
                $Tag3         = new Tags('div');
                $Tag3->class  = 'info-client_error-text';
                $Tag4         = new Tags('p');
                $Tag4->add("Oops, we've encountered a technical problem. This error has been recorded by our system and will be investigated by our support team. Please try clearing your cache and reloading the Hotel.");
                $Tag3->add($Tag4);
                $Tag5         = new Tags('p');
                $Tag5->add("Please re-open <a href=\"{{hotel_url}}{{client_name}}\">hotel</a> to continue. We are sorry for the inconvenience.");
                $Tag3->add($Tag5);
                $Tag7         = new Tags('div');
                $Tag7->class  = 'retry-enter-hotel';
                $Tag8         = new Tags('div');
                $Tag8->class  = 'hotel-open';
                $Tag9         = new Tags('a');
                $Tag9->id     = 'enter-hotel-open-image';
                $Tag9->class  = 'open';
                $Tag9->href   = "{{hotel_url}}{{client_name}}";
                $Tag10        = new Tags('div');
                $Tag10->class = 'hotel-open-image-splash';
                $Tag10->add('');
                $Tag11        = new Tags('div');
                $Tag11->class = 'hotel-image hotel-open-image';
                $Tag11->add('');
                $Tag12        = new Tags('div');
                $Tag12->class = 'hotel-open-button-content';
                $Tag13        = new Tags('a');
                $Tag13->class = 'open';
                $Tag13->href  = "{{hotel_url}}{{client_name}}";
                $Tag13->add('Enter Habbo Hotel');
                $Tag14        = new Tags('span');
                $Tag14->class = 'open';
                $Tag14->add('');
                $Tag12->add($Tag13);
                $Tag12->add($Tag14);
                $Tag9->add($Tag10);
                $Tag9->add($Tag11);
                $Tag8->add($Tag9);
                $Tag8->add($Tag12);
                $Tag7->add($Tag8);
                $Tag20        = new Tags('div');
                $Tag20->add($Tag3);
                $Tag20->add($Tag7);
                $Tag6         = Content::HabbleContainer('v3_darkblue_glow', $Tag20, 'Ops!!');
                $Tag2->add($Tag6);
                $Body->AddContent($Tag1);
                $Codes        = $Body->EndBody();
                break;
            case "register":
                if (!isset($Code))
                    $Code         = "";
                $Code .= Body::Overlay(1);
                $Code .= "<div id=\"change-password-form\" class=\"overlay-dialog\" style=\"left: 40%; top: 80px;\">";
                $Code .= "<div id=\"change-password-form-container\" class=\"clearfix form-container\">\n";
                $Code .= "<h2 id=\"change-password-form-title\" class=\"bottom-border\">Register on {{hotel_name}}</h2>";
                $Code .= "<div id=\"change-password-form-content\" style=\"\">";
                $Code .= "<style>.email-address {font-size: 18px;font-weight: bold;height: 30px;width: 330px;text-align: center;color: black;border: 1px solid #633;}</style>";
                $Code .= "<form action=\"{{hotel_url}}?p=register&a=register\" method=\"post\" id=\"forgotten-pw-form\">\n";
                $Code .= "<div id=\"change-password-form-content\">\n";
                $Code .= "<span>{{ErrorMessage}}</span><br>";
                $Code .= "<span>Type your Username</span>";
                $Code .= "<div id=\"email\" class=\"center bottom-border\">";
                $Code .= "<input tabindex=\"1\" type=\"text\" name=\"username\" id=\"change-password-email-address\" class=\"email-address\" maxlength=\"48\" style=\"width:200px;\"/>\n";
                $Code .= "</div>\n";
                $Code .= "<span>Type your Mail</span>";
                $Code .= "<div id=\"email\" class=\"center bottom-border\">";
                $Code .= "<input tabindex=\"1\" type=\"text\" name=\"mail\"  id=\"change-password-email-address\" class=\"email-address\" maxlength=\"48\" style=\"width:200px;\" />\n";
                $Code .= "</div>\n";
                $Code .= "<span>Type your Password</span>";
                $Code .= "<div id=\"email\" class=\"center bottom-border\">";
                $Code .= "<input tabindex=\"2\" type=\"password\" name=\"password\" id=\"change-password-email-address\" class=\"email-address\" maxlength=\"48\" style=\"width:200px;\"/>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<p></p>\n";
                $Code .= "<div class=\"change-password-buttons\">";
                $Code .= "<a href=\"?p=index\" id=\"change-password-cancel-link\">Cancel</a>";
                $Code .= "<a href=\"javascript:;\" onclick=\"document.getElementById('forgotten-pw-form').submit();\" id=\"change-password-submit-button\" class=\"new-button\"><b>Ok</b><i></i></a>\n";
                $Code .= "</form></div></div>\n";
                $Code .= "</div><div id=\"change-password-form-container-bottom\" class=\"form-container-bottom\"></div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "function initChangePasswordForm() { ChangePassword.init(); } if (window.HabboView) { HabboView.add(initChangePasswordForm); } else if (window.habboPageInitQueue) { habboPageInitQueue.push(initChangePasswordForm); }";
                $Code .= "</script>\n";
            case "index":
                if (!isset($Code))
                    $Code         = "";
                if (StorageHabbo::getHabbo('Id') != "not_logged"):
                    header("Location: ?p=me");
                endif;
                $Code .= "<div class=\"cbb process-template-box clearfix\" style=\"border-radius: 6px;border-bottom: 3px solid rgb(150, 179, 190);width: 820px;height: 600px;background: url('http://www.ihabbo.nu/img/bg.png') no-repeat right bottom white;\">\n";
                $Code .= "<div id=\"content\">\n";
                $Code .= "<div id=\"header\" class=\"clearfix\">\n";
                $Code .= "<h1><a href=\"?p=index\" style=\"color: #008D49;font-family: 'Open Sans', sans-serif;font-weight: lighter;font-size: 35px;text-decoration: none;background-image: none;text-indent: 0;\">{{hotel_name}}</a></h1>";
                $Code .= "<ul class=\"stats\">\n";
                $Code .= "<li class=\"stats-online\"><span class=\"stats-fig\">{{online_users}}</span> Users Online</li>\n";
                $Code .= "<li class=\"stats-visited\"><span class=\"stats-fig\">{{registered_users}}</span> Registered Users</li>\n";
                $Code .= "</ul>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"process-content\">\n";
                $Code .= "<div id=\"column2\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \" id=\"create-habbo\">\n";
                $Code .= "<div id=\"create-habbo-flash\">\n";
                $Code .= "<div id=\"create-habbo-nonflash\">\n";
                $Code .= "<div id=\"landing-register-text\"><a href=\"javascript:window.location.href='{{hotel_url}}' + 'p?=register'\"><span>Join now, it's free</span></a></div>\n";
                $Code .= "<div id=\"landing-promotional-text\"><span>Habbo is a virtual world where you can meet and make friends.</span></div>\n";
                $Code .= "</div>\n";
                $Code .= "<div class=\"cbb clearfix green\" id=\"habbo-intro-nonflash\">\n";
                $Code .= "<h2 class=\"title\">To get most out of Habbo, do this:</h2>\n";
                $Code .= "<div class=\"box-content\">\n";
                $Code .= "<ul>\n";
                $Code .= "<li id=\"habbo-intro-install\" style=\"display:none\"><a href=\"http://www.adobe.com/go/getflashplayer\">Install Flash Player 8 or higher</a></li>\n";
                $Code .= "<noscript><li>Enable JavaScript</li></noscript>\n";
                $Code .= "</ul>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "var swfobj = new SWFObject(\"{{gallery_url}}flash/intro/habbos.swf\", \"ch\", \"396\", \"378\", \"8\");\n";
                $Code .= "swfobj.addParam(\"AllowScriptAccess\", \"always\");\n";
                $Code .= "swfobj.addParam(\"wmode\", \"transparent\");\n";
                $Code .= "swfobj.addVariable(\"base_url\", \"{{gallery_url}}flash/intro/\");\n";
                $Code .= "swfobj.addVariable(\"habbos_url\", \"{{hotel_url}}?a=promo\");\n";
                $Code .= "swfobj.addVariable(\"create_button_text\", \"Register Today!\");\n";
                $Code .= "swfobj.addVariable(\"in_hotel_text\", \"Online now!\");\n";
                $Code .= "swfobj.addVariable(\"slogan\", \"Habbo\");\n";
                $Code .= "swfobj.addVariable(\"video_start\", \"PLAY VIDEO\");\n";
                $Code .= "swfobj.addVariable(\"video_stop\", \"STOP VIDEO\");\n";
                $Code .= "swfobj.addVariable(\"button_link\", \"?p=register\");\n";
                $Code .= "swfobj.addVariable(\"localization_url\", \"{{gallery_url}}flash/intro/landing_intro.xml\");\n";
                $Code .= "swfobj.addVariable(\"video_link\", \"{{gallery_url}}flash/intro/Habbo_intro.swf\");\n";
                $Code .= "swfobj.write(\"create-habbo-flash\");\n";
                $Code .= "HabboView.add(function() {\n";
                $Code .= "if(deconcept.SWFObjectUtil.getPlayerVersion()[\"major\"] >= 8) {\n";
                $Code .= "try { $(\"habbo-intro-nonflash\").hide(); } catch (e) {}\n";
                $Code .= "} else {\n";
                $Code .= "$(\"habbo-intro-install\").show();\n";
                $Code .= "}\n";
                $Code .= "});\n";
                $Code .= "var PromoHabbos = { track:function(n) { if (!!n && window.pageTracker) { pageTracker._trackPageview(\"/landingpromo/\" + n); } } }\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName(\'process-template\')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"column3\" class=\"column\" style=\"width: 158px;margin-top: -8px;\">\n";
                $Code .= "<div class=\"cbb loginbox clearfix\" style=\"border-radius: 3px;border: 1px solid rgb(204, 204, 204);border-bottom: 3px solid rgb(204, 204, 204);\">\n";
                $Code .= "<h2 class=\"title\" style=\"margin: 4px;border-radius: 3px;\">Login</h2>\n";
                $Code .= "<div class=\"box-content clearfix\" id=\"login-habblet\">\n";
                $Code .= "<form action=\"{{hotel_url}}?p=index&a=login\" method=\"post\" class=\"login-habblet\">\n";
                $Code .= "<span>{{ErrorMessage}}</span>";
                $Code .= "<ul>\n";
                $Code .= "<li>\n";
                $Code .= "<label for=\"login-username\" class=\"login-text\">Username</label>\n";
                $Code .= "<input tabindex=\"1\" type=\"text\" class=\"login-field\" name=\"username\" id=\"login-username\" />\n";
                $Code .= "</li>\n";
                $Code .= "<li>\n";
                $Code .= "<label for=\"login-password\" class=\"login-text\">Password</label>\n";
                $Code .= "<input tabindex=\"2\" type=\"password\" class=\"login-field\" name=\"password\" id=\"login-password\" />\n";
                $Code .= "<input type=\"submit\" style=\"display:none;\" value=\"Sign in\" class=\"submit\" id=\"login-submit-button\"/>\n";
                $Code .= "</li>\n";
                $Code .= "<li class=\"no-label\" style=\"margin-left: 22px;padding:0px;margin-top: 45px;\">\n";
                $Code .= "<a href=\"javascript:window.location.href='{{hotel_url}}' + '?p=register'\"  class=\"login-register-link\"><span>Register</span></a>\n";
                $Code .= "</li>\n";
                $Code .= "</ul>\n";
                $Code .= "</form>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"remember-me-notification\" class=\"bottom-bubble\" style=\"display:none;\">\n";
                $Code .= "<div class=\"bottom-bubble-t\"><div></div></div>\n";
                $Code .= "<div class=\"bottom-bubble-c\">\n";
                $Code .= "By selecting \'remember me\' you will stay signed in on this computer until you click \'Sign Out\'. If this is a public computer please do not use this feature.\n";
                $Code .= "</div>\n";
                $Code .= "<div class=\"bottom-bubble-b\"><div></div></div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.add(LoginFormUI.init);\n";
                $Code .= "HabboView.add(RememberMeUI.init);\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "createnews":
                if (!isset($Code))
                    $Code  = "";
                if (StorageHabbo::getHabbo('IsAdmin')):
                    $Code .= Body::Overlay(1);
                    $Code .= "<script src=\"//cdn.ckeditor.com/4.4.6/standard/ckeditor.js\"></script>";
                    $Code .= "<div id=\"change-password-form\" class=\"overlay-dialog\" style=\"left: 40%; top: 80px;\">";
                    $Code .= "<div id=\"change-password-form-container\" class=\"clearfix form-container\">\n";
                    $Code .= "<h2 id=\"change-password-form-title\" class=\"bottom-border\">Create a New Article</h2>";
                    $Code .= "<div id=\"change-password-form-content\" style=\"\">";
                    $Code .= "<style>.email-address {font-size: 18px;font-weight: bold;height: 30px;width: 330px;text-align: center;color: black;border: 1px solid #633;}</style>";
                    $Code .= "<div id=\"change-password-form-content\">\n";
                    $Code .= "<span>{{SuccessMessage}} {{ErrorMessage}}<br></span>";
                    if (!isset($_GET['a'])):
                        $Code .= "<form action=\"{{hotel_url}}?p=createnews&a=postarticle\" method=\"post\" id=\"forgotten-pw-form\">\n";
                        $Code .= "<span>Type the Title</span>";
                        $Code .= "<div id=\"email\" class=\"center bottom-border\">";
                        $Code .= "<input tabindex=\"1\" type=\"text\" name=\"title\" id=\"change-password-email-address\" class=\"email-address\" style=\"width:320px;\"/>\n";
                        $Code .= "</div>\n";
                        $Code .= "<span>Type the Image Url</span>";
                        $Code .= "<div id=\"email\" class=\"center bottom-border\">";
                        $Code .= "<input tabindex=\"1\" type=\"text\" name=\"image\"  id=\"change-password-email-address\" class=\"email-address\" style=\"width:320px;\" />\n";
                        $Code .= "</div>\n";
                        $Code .= "<span>Type The Text</span>";
                        $Code .= "<div id=\"email\" class=\"center bottom-border\">";
                        $Code .= "<textarea name=\"text\" id=\"change-password-email-address\" class=\"email-address\" style=\"width:320px;height:100px;\"></textarea>\n";
                        $Code .= "<script>
					CKEDITOR.replace( 'text', {
					language: 'en',
					width: 320,
					height: 145
					});</script>";
                        $Code .= "</div>\n";
                        $Code .= "</div>\n";
                        $Code .= "<p>&nbsp;</p>\n";
                        $Code .= "<div class=\"change-password-buttons\">";
                        $Code .= "<a href=\"?p=me\" id=\"change-password-cancel-link\">Cancel</a>";
                        $Code .= "<a href=\"javascript:;\" onclick=\"document.getElementById('forgotten-pw-form').submit();\" id=\"change-password-submit-button\" class=\"new-button\"><b>Ok</b><i></i></a>\n";
                        $Code .= "</form>";
                    else:
                        $Code .= "<a href=\"?p=me\" id=\"change-password-cancel-link\">Fechar</a>";
                    endif;
                    $Code .= "</div></div>\n";
                    $Code .= "</div><div id=\"change-password-form-container-bottom\" class=\"form-container-bottom\"></div>\n";
                    $Code .= "</div>\n";
                    $Code .= "<script type=\"text/javascript\">\n";
                    $Code .= "function initChangePasswordForm() { ChangePassword.init(); } if (window.HabboView) { HabboView.add(initChangePasswordForm); } else if (window.habboPageInitQueue) { habboPageInitQueue.push(initChangePasswordForm); }";
                    $Code .= "</script>\n";
            endif;
            case "me":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= "<div data-alert=\"\" style=\"background-color: #43ac6a;color: white;border-style: solid;border-width: 1px;display: block;font-weight: normal;position: relative;padding: 0.77778rem 1.33333rem 0.77778rem 0.77778rem;font-size: 0.72222rem;transition: opacity 300ms ease-out;width: 725px;border-color: #3a945b;margin-left: 2px;border-radius: 3px;margin-bottom:6px;\">Hello <b>{{name}}</b> The <i>{{hotel_name}}</i> Is On Development..</div>";
                $Code .= "<div id=\"wide-personal-info\">\n";
                $Code .= "<div id=\"habbo-plate\">\n";
                $Code .= "<a href=\"#\">\n";
                $Code .= "<img src=\"http://habbo.de/habbo-imaging/avatarimage?figure={{look}}&size=2rg&direction=4&head_direction=3&gesture=sml&size=g\"\>";
                $Code .= "</a>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"name-box\" class=\"info-box\">\n";
                $Code .= "<div class=\"label\">Name:</div>\n";
                $Code .= "<div class=\"content\">{{name}}</div>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"motto-box\" class=\"info-box\">\n";
                $Code .= "<div class=\"label\">Motto:</div>\n";
                $Code .= "<div class=\"content\">{{motto}}</div>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"last-logged-in-box\" class=\"info-box\">\n";
                $Code .= "<div class=\"label\">Last signed in:</div>\n";
                $Code .= "<div class=\"content\">Undefined</div>\n";
                $Code .= "</div>\n";
                $Code .= "<div class=\"enter-hotel-btn\">\n";
                $Code .= "<div class=\"open enter-btn\">\n";
                $Code .= "<a href=\"{{hotel_url}}{{client_name}}\" style=\"border-style: solid;border-width: 0px;cursor: pointer;font-weight: normal;line-height: normal;margin: 0 0 1.11111rem;position: relative;text-decoration: none;text-align: center;-webkit-appearance: none;display: inline-block;padding-top: 0.88889rem;padding-right: 1.77778rem;padding-bottom: 0.94444rem;padding-left: 1.77778rem;font-size: 0.88889rem;background-color: #f04124;border-color: #cf2a0e;color: white;border-radius: 3px;top: 30px;background-image:none;height:auto;\">Enter Habbo Hotel</a>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= StorageHabbo::News();
                $Code .= "<div id=\"column1\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \" style=\"visibility:{{twitter_on}};\">		\n";
                $Code .= "<div id=\"twitterfeed-habblet-container\">\n";
                $Code .= "<a class=\"twitter-timeline\" height=\"250\" data-dnt=\"true\" href=\"https://twitter.com/{{twitter_name}}\" data-widget-id=\"{{twitter_id}}\">Tweets by @Habbo</a>\n";
                $Code .= "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"column2\" class=\"column\">\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"column3\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \">		\n";
                $Code .= "<div class=\"ad-container\">\n";
                $Code .= "<img src=\"http://images.habbo.com/images/477/Ad10840029St1Sz1778Sq106346171V0Id1.png\" border=\"0\" alt=\" \" width=\"160\" height=\"110\">\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "rares":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= "<div id=\"habblet-container\" class=\"column\"> 		     		\n";
                $Code .= "<div class=\"habblet-container \">		\n";
                $Code .= "<div class=\"cbb clearfix white \"> \n";
                $Code .= "<h2 class=\"title\">Habbo Rare Values						</h2> \n";
                $Code .= "<div class=\"habblet-container\"> \n";
                $Code .= "<div class=\"pixels-infobox-text\"> \n";
                $Code .= "<center><b>never</b> get <b>scammed</b> again, by following our values!</i></center> \n";
                $Code .= "<ul> \n";
                $Code .= "<li><p>\n";
                $Code .= "<table width=\"100%\" border=\"0\">\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:center\"><b>Image</b></td>\n";
                $Code .= "<td width=\"45%\" style=\"text-align:left\"><b>Item Name</b></td>\n";
                $Code .= "<td width=\"40%\" style=\"text-align:center\"><b>Cost</b></td>\n";
                $Code .= "<td width=\"10%\" style=\"text-align:center\"><b>Status</b></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/throne.gif\"></td>\n";
                $Code .= "<td width=\"35%\" style=\"text-align:left;background-color:#f5f5f5\">Throne</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>95000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/up.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/cola.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Cola Machine</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>7000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/typo.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Typewriter</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>15000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/hammock.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Hammock</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>6000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/aloe.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Aloe Vera</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>1200</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt='''' src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/amber_blue.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Blue Amber Lamp</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>5000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/amber_red.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Red Amber Lamp</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>4500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/amber_yellow.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Golden Amber Lamp</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>5500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/bath_blue.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Blue Bird Bath</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>4000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/bath_red.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Red Bird Bath</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>3500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/bath_grey.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Grey Bird Bath</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>4500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/dragon_black.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Black Dragon</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>40000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/up.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/dragon_jade.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Jade Dragon</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>35000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/icm_brown.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Chocolate Ice-cream</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>30000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/laser_black.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Black Laser</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>25000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/mono_white.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">White Mono</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>15000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/para_purple.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Purple Parasol</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>5000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/nelly_silver.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Silver Nelly</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>15000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/nelly_gold.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Golden Nelly</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>25000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/up.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/dino.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Dragon Egg</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>30000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/up.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/dragon_silver.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Silver Dragon</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>25000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/fan_yellow.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Yellow Fan</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>5000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/marquee_green.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Green Marquee</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>10000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/hologirl.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Holo-girl</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>20000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/holopod.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Holo-Boy</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>20000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/up.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/pillar_blue.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Blue Pillar</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>10000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/gallery_infobus.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Info bus</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>4000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/monsterplant.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Monster Plant</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>5000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/slurpee_red.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Red Slurpee</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>4000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/nelly_bronze.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Bronze Nelly</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>20000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/dragon_bronze.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Bronze Dragon</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>20000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/laser_red.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Red Laser</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>45000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/Dragon_sky.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Sky Dragon</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>65000</b>  Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/Dragon_blue.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Sea Dragon</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>55000</b>  Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/laser_yellow.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Yellow Laser</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>80000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/marquee_red.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Red Marquee</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>10000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/Petal.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Petal Patch</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>75000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/pillar_brown.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Brown Pillar</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>5000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/snow.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Snow Patch</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>45000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/bath_green.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Green Bird Bath</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>20000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/globe.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">Snow Globe</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>15000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/screen_red.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Red Screen</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>10000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/Pillar_white.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">White Pillar</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>10000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/pillow_white.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">White Pillow</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>40000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/screen_white.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">White Screen</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>5500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/slurpee_green.gif \"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Green Calipo</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>2500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#ffffff\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/smoke_white.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#ffffff\">White Smoke</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#ffffff\"><b>15000</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td width=\"15%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/rares/marquee_blue.gif\"></td>\n";
                $Code .= "<td width=\"25%\" style=\"text-align:left;background-color:#f5f5f5\">Blue Marquee</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><b>5500</b> Credits</td>\n";
                $Code .= "<td width=\"20%\" style=\"text-align:center;background-color:#f5f5f5\"><img alt=\"\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/values/images/same.png\"></td>\n";
                $Code .= "</tr>\n";
                $Code .= "</table>\n";
                $Code .= "</p></li> \n";
                $Code .= "</ul> \n";
                $Code .= "<p</p> \n";
                $Code .= "<p><a href=\"javascript:void(0);\" target=\"_blank\"></a></p> \n";
                $Code .= "</div> \n";
                $Code .= "</div> 	\n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script> \n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "articles":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= StorageHabbo::ArticleData();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "donate":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= "<div id=\"column1\" class=\"column\"><div class=\"habblet-container \">\n";
                $Code .= "<div class=\"cbb clearfix blue \">\n";
                $Code .= "<h2 class=\"title\">Donation Information</h2>\n";
                $Code .= "<div id=\"habboclub-info\" class=\"box-content\">\n";
                $Code .= "<img src=\"{{hotel_url}}{{gallery_url}}/v2/images/geschenke.gif\" align=\"right\" vspace=\"5\" hspace=\"5\">\n";
                $Code .= "<p>\n";
                $Code .= "<h3><font color='darkred'>Why donate?</h3><font color='black'>\n";
                $Code .= "Fame Hotel runs on donations from users like you! Without users constantly donating and supporting us, we would not be able to stay online. In addition to knowing that Fame Hotel will be able to live on even longer, you can also get some pretty cool rewards for donating! Look at the list to the right to see those rewards.\n";
                $Code .= "<h3><font color='darkred'>How do I donate?</h3><font color='black'>\n";
                $Code .= "Donating is similar to purchasing VIP. The only payment method that is available for donating is Paypal. Simply scroll to the bottom of the page, select the donation amount, enter in your Fame Hotel username, and click pay now! Paypal is the leading and most widely-used payment system on the internet. After donating through Paypal, you can expect to have your credits, pixels, badge, and rares delivered to you instantly, or at most a few minutes. The rares will be sent to you in Red Presents. You can simply place these presents down and open them to receive your rares.\n";
                $Code .= "<h3><font color='darkred'>Can I donate more than once?</h3><font color='black'>\n";
                $Code .= "You can donate as many times as you like! Donate whenever you can -- we can always use more funds to help pay for our multiple servers, domains, and various other services which we provide free of charge.\n";
                $Code .= "<h3><font color='darkred'>Refunds?</h3><font color='black'>\n";
                $Code .= "Refunds are not allowed. <b>All payments are final.</b> By donating to Fame Hotel, you agree that you will not attempt to refund your donation at any time. This causes a huge hassle for us, and we <b>will</b> ban your account if you attempt to get your money back.<p>\n";
                $Code .= "<h3><font color='darkred'>What color rares are available?</h3><font color='black'>\n";
                $Code .= "<b>Ice Cream Machines</b> - <i>Aqua, Blue, Choco, Fucsia, Gold, Ochre, Pink, Purple, Red, Shamrock</i><br>\n";
                $Code .= "<b>Slurpee Machines</b> - <i>Aqua, Blueberry, Cherry, Grape, Lime, Orange, Pink</i><br>\n";
                $Code .= "<b>Arcade Machines</b> - <i>Red, Yellow</i><br><br>\n";
                $Code .= "All rare colors will be randomly selected. You have an equal chance to receive all colors of rares!\n";
                $Code .= "<br><br><p>So why wait?! Donate now, and get some really cool rewards!</p><font color='black'>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script><div class=\"habblet-container \">\n";
                $Code .= "<div class=\"cbb clearfix white \" style=\"padding: 15px;\">\n";
                $Code .= "<center>                <a onclick=\"window.open('/buy_donate.php','popup','width=900,height=800,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false\" href=\"{{paypalurl}}\" target=\"_blank\" class=\"new-button habboid-submit\" style=\"float: left;\"><b>Pay By Paypal</b><i></i></a>\n";
                $Code .= "</center>\n";
                $Code .= "</p>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script></div><div id=\"column2\" class=\"column\"><div class=\"habblet-container \">\n";
                $Code .= "<div class=\"cbb clearfix settings \">\n";
                $Code .= "<h2 class=\"title\">Donation Benefits</h2>\n";
                $Code .= "<div id=\"habboclub-info\" class=\"box-content\">\n";
                $Code .= "<p><strong><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX00.gif\" alt=\"\" width=\"40\" height=\"40\"/>Level 1 (<em>£1.00) </em></strong><br>Donator's Badge (Level 1), 10,000 Credits & 5000 pixels </p>\n";
                $Code .= "<p><strong><span style=\"font-weight: normal;\"><strong><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX01.gif\" alt=\"\" width=\"40\" height=\"40\"/>Level 2 (<em>£3.00) &nbsp;</em></strong><br>Donator's Badge (Level 2), , 10000 credits, 3000 pixels</span></strong></p>\n";
                $Code .= "<p><strong><span style=\"font-weight: normal;\"><strong><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX02.gif\" alt=\"\" width=\"40\" height=\"40\"/>Level 3 (<em>£5.00) &nbsp;</em></strong><br>Donator's Badge (Level 3), , 20000 credits, 6000 pixels, 1x any color ice-cream machine</span></strong></p>\n";
                $Code .= "<p><strong><span style=\"font-weight: normal;\"><strong><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX03.gif\" alt=\"\" width=\"40\" height=\"40\"/>Level 4 (<em>£7.00) &nbsp;</em></strong><br>Donator's Badge (Level 4), , 40000 credits, 13000 pixels, 2x any color ice-cream machine, 1x throne</span></strong></p>\n";
                $Code .= "<p><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX04.gif\" alt=\"\" width=\"40\" height=\"40\"/><strong><span style=\"font-weight: normal;\"><strong>Level 5 (<em>£9.00) &nbsp;</em></strong><br>Donator's Badge (Level 5), , 60000 credits, 20000 pixels, 3x any color ice-cream machine, 2x throne, 1x lollipop stand</span></strong></p>\n";
                $Code .= "<p><strong><span style=\"font-weight: normal;\"><strong><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX05.gif\" alt=\"\" width=\"40\" height=\"40\"/></strong></span></strong><strong><span style=\"font-weight: normal;\"><strong>Level 6 (<em> £15.00) &nbsp;</em></strong><br>Donator's Badge (Level 6), , 80000 credits, 26000 pixels, 4x any color ice-cream machine, 3x throne, 1x lollipop stand, 1x any color slurpee machine</span></strong></p>\n";
                $Code .= "<p><strong><span style=\"font-weight: normal;\"><strong><img align=\"middle\" src=\"{{hotel_url}}{{gallery_url}}/v2/images/PX06.gif\" alt=\"\" width=\"40\" height=\"40\"/></strong></span></strong><strong><span style=\"font-weight: normal;\"><strong>Level 7 (<em>£18.00) &nbsp;</em></strong><br>Donator's Badge (Level 7), , 100000 credits, 33000 pixels, 5x any color ice-cream machine, 4x throne, 1x lollipop stand, 1x any color slurpee machine, 1x arcade machine</span></strong></p>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "Pngfix.doPngImageFix();\n";
                $Code .= "</script>\n";
                $Code .= "<![endif]-->\n";
                $Code .= "<div id=\"footer\">\n";
                $Code .= "<div style=\"float: center;\">\n";
                $Code .= "</div>\n";
                $Code .= "<div style=\"float: right;\">\n";
                $Code .= "</div>\n";
                $Code .= "<div style=\"clear: both;\"></div>\n";
                $Code .= "</div> </div>\n";
                $Code .= "</div></div>\n";
                $Code .= "<script type=\"text/javascript\"> \n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "welcome":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<div id=\"column1\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \">\n";
                $Code .= "<div class=\"cbb clearfix red\">\n";
                $Code .= "<h2 class=\"title\">A Personalized Room For You\n";
                $Code .= "</h2>\n";
                $Code .= "<ul class=\"roomselection-welcome clearfix\">\n";
                $Code .= "<li class=\"odd\">\n";
                $Code .= "<a class=\"roomselection-select new-button\" href=\"{{hotel_url}}{{client_name}}&createRoom=0\"><b>Select</b><i></i></a>\n";
                $Code .= "</li>\n";
                $Code .= "<li class=\"even\">\n";
                $Code .= "<a class=\"roomselection-select new-button\" href=\"c{{hotel_url}}{{client_name}}&createRoom=1\"><b>Select</b><i></i></a>\n";
                $Code .= "</li>\n";
                $Code .= "<li class=\"odd\">\n";
                $Code .= "<a class=\"roomselection-select new-button\" href=\"{{hotel_url}}{{client_name}}&createRoom=2\"><b>Select</b><i></i></a>\n";
                $Code .= "</li>\n";
                $Code .= "<li class=\"even\">\n";
                $Code .= "<a class=\"roomselection-select new-button\" href=\"{{hotel_url}}{{client_name}}&createRoom=3\"><b>Select</b><i></i></a>\n";
                $Code .= "</li>\n";
                $Code .= "<li class=\"odd\">\n";
                $Code .= "<a class=\"roomselection-select new-button\" href=\"{{hotel_url}}{{client_name}}&createRoom=4\"><b>Select</b><i></i></a>\n";
                $Code .= "</li>\n";
                $Code .= "<li class=\"even\">\n";
                $Code .= "<a class=\"roomselection-select new-button\" href=\"{{hotel_url}}{{client_name}}&createRoom=5\"><b>Select</b><i></i></a>\n";
                $Code .= "</li>\n";
                $Code .= "</ul>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"column2\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \">\n";
                $Code .= "<div class=\"cbb clearfix notitle\">\n";
                $Code .= "<div class=\"welcome-intro clearfix\">\n";
                $Code .= "<img alt=\"Prince-Cutie9\" src=\"http://www.habbo.de/habbo-imaging/avatarimage?figure={{look}}&size=b&action=crr=667&direction=3&head_direction=3&gesture=srp\n \"width=\"64\" height=\"110\" class=\"welcome-habbo\" />\n";
                $Code .= "<div id=\"welcome-intro-welcome-user\"  >Welcome, {{name}}!</div>\n";
                $Code .= "<div id=\"welcome-intro-welcome-party\" class=\"box-content\">It's Really Good Have You on {{hotel_name}}!</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "staff":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= "<div id=\"column1\" class=\"column\">\n";
                $Code .= "<div class=\"cbb clearfix red\">\n";
                $Code .= "<div class=\"box-content\">\n";
                $Code .= StorageHabbo::Staffs();
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<div id=\"column2\" class=\"column\">\n";
                $Code .= "<div class=\"cbb clearfix settings\">\n";
                $Code .= "<h2 class=\"title\">What's the {{hotel_name}} Staff?</h2>\n";
                $Code .= "<div class=\"box-content\">\n";
                $Code .= "<div class=\"box-content\"><br><img src=\"http://i.imgur.com/mA8Tm.png\" style=\"float: left;\"><span style=\"\">{{hotel_name}} Staffs Makes the Hotel Better, Managing the Hotel and The Security of The Hotel. Promoving Events And More</span></div>";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "credits":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= "<div id=\"column1\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \">		\n";
                $Code .= "<div class=\"cbb clearfix orange \"> \n";
                $Code .= "<h2 class=\"title\">Get {{hotel_name}} Credits</h2> \n";
                $Code .= "<div class=\"method-group online clearfix\"> \n";
                $Code .= "<div class=\"method idx0\"> \n";
                $Code .= "<div class=\"method-content\"> 		\n";
                $Code .= "<h2>Free credits!</h2> \n";
                $Code .= "<div class=\"summary clearfix\">    \n";
                $Code .= "{{hotel_name}} Credits are completely FREE, and always will be. On registration, you get 15,000 for free, awesome bro!\n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "<div class=\"smallprint\"> \n";
                $Code .= "<style type=\"text/css\">.method-group.online .method.idx0 h2 {font-size:26px !important;}</style> \n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "<div class=\"method idx1 nosmallprint\"> \n";
                $Code .= "<div class=\"method-content\"> \n";
                $Code .= "<h2>How do I gain more credits?</h2> \n";
                $Code .= "<div class=\"summary clearfix\"> \n";
                $Code .= "<div id=\"credits-countdown\">\n";
                $Code .= "You can gain more {{hotel_name}} credits by either buying VIP or using your current amount and playing the economy in the hotel.	\n";
                $Code .= "</div>\n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "<script type=\"text/javascript\"> \n";
                $Code .= "document.observe(\"dom:loaded\", function() { new CreditsList(); });\n";
                $Code .= "</script> \n";
                $Code .= "<div class=\"disclaimer\" style=\"border: 0 !important;\"> \n";
                $Code .= "<h3><span>Do you have any questions?</span></h3> \n";
                $Code .= "Are you out of credits? That's weird! We have events in the hotel constantly being run by our staff<br />\n";
                $Code .= "where you can win rare furniture and coins. You can also go to one of the hotel casinos and try your luck there.<br />\n";
                $Code .= "It's really quite fun to play your luck at a casino, so give it a try!<br /><br />\n";
                $Code .= "<strong>If you have any more questions, feel free to contact a member of our staff.</strong>\n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "</div> \n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script> \n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "safety":
                if (!isset($Code))
                    $Code  = "";
                $Code .= "<span style=\"width:753px !important;\">{{ErrorMessage}} {{SuccessMessage}}</span><br>";
                $Code .= "<div id=\"column1\" class=\"column\">\n";
                $Code .= "<div class=\"habblet-container \">\n";
                $Code .= "<div id=\"habbo-way-content\">\n";
                $Code .= "<table>\n";
                $Code .= "<tbody><tr>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_0.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Protect Your Personal Info</h4>\n";
                $Code .= "You never know who you're really speaking to online, so never give out your real name, address, phone numbers, photos and school. Giving away your personal info could lead to you being scammed, bullied or put in danger.\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_1.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Protect Your Privacy</h4>\n";
                $Code .= "Keep your Skype, MSN, Facebook details private. You never know where it might lead you.\n";
                $Code .= "</td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_2.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Don't Give In To Peer Pressure</h4>\n";
                $Code .= "Just because everyone else seems to be doing it, if you're not comfortable with it, don't do it!\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_3.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Keep Your Pals In Pixels</h4>\n";
                $Code .= "Never meet up with people you only know from the internet, people aren't always who they claim to be. If someone asks you to meet with them in real life say \"No thanks!\" and tell a moderator, your parents or another trusted adult.\n";
                $Code .= "</td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_4.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Don't Be Scared To Speak Up</h4>\n";
                $Code .= "If someone is making you feel uncomfortable or scaring you with threats in Habbo, report them immediately to a moderator using the Panic Button.\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_5.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Ban The Cam</h4>\n";
                $Code .= "You have no control over your photos and webcam images once you share them over the internet and you can't get them back. They can be shared with anyone, anywhere and be used to bully or blackmail or threaten you. Before you post a pic, ask yourself, are you comfortable with people you don't know viewing it?\n";
                $Code .= "</td>\n";
                $Code .= "</tr>\n";
                $Code .= "<tr>\n";
                $Code .= "<td>\n";
                $Code .= "<img src=\"//habboo-a.akamaihd.net/c_images/safetyquiz/page_6.png\" alt=\"\"> <br>\n";
                $Code .= "</td>\n";
                $Code .= "<td>\n";
                $Code .= "<h4>Be A Smart Surfer</h4>\n";
                $Code .= "Websites that offer you free Credits, Furni, or pretend to be new Habbo Hotel or Staff homepages are all scams designed to steal your password. Don't give them your details and never download files from them; they could be keyloggers or viruses!\n";
                $Code .= "</td>\n";
                $Code .= "</tr>\n";
                $Code .= "</tbody></table>\n";
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
                $Code .= "</div>\n";
                $Code .= "<script type=\"text/javascript\">\n";
                $Code .= "HabboView.run();\n";
                $Code .= "</script>\n";
                $Code .= "</div>\n";
                $Code .= Page::Footer();
                $Code .= "</div>\n";
                $Code .= "</div>\n";
                $Code .= "\n";
                $Codes = $Body->GenericBody($Code);
                break;
            case "client":
                if (!isset($Code))
                    $Code  = "";
                if (!StorageHabbo::$NeedDatabase):
                    StorageHabbo::$NeedDatabase = true;
                endif;
                $Code .= "            <script type=\"text/javascript\">\n";
                $Code .= "                document.habboLoggedIn = true;\n";
                $Code .= "                var habboName = \"{{name}}\";\n";
                $Code .= "                var habboId = {{id}};\n";
                $Code .= "                var facebookUser = true;\n";
                $Code .= "                var habboReqPath = \"\";\n";
                $Code .= "                var habboStaticFilePath = \"{{hotel_url}}{{gallery_url}}\";\n";
                $Code .= "                var habboImagerUrl = \"{{hotel_url}}habbo-imaging/\";\n";
                $Code .= "                var habboPartner = \"\";\n";
                $Code .= "                var habboDefaultClientPopupUrl = \"{{hotel_url}}{{client_name}}\";\n";
                $Code .= "                window.name = \"\";\n";
                $Code .= "                if (typeof HabboClient != \"undefined\") {\n";
                $Code .= "                    HabboClient.windowName = \"\";\n";
                $Code .= "                    HabboClient.maximizeWindow = true;\n";
                $Code .= "                }\n";
                $Code .= "            </script>\n";
                $Code .= "\n";
                $Code .= "            <link rel=\"stylesheet\" href=\"{{hotel_url}}{{gallery_url}}static/styles/habboflashclient.css\" type=\"text/css\" />\n";
                $Code .= "            <script src=\"{{hotel_url}}{{gallery_url}}static/js/habboflashclient.js\" type=\"text/javascript\"></script>\n";
                $Code .= "            <script src=\"{{hotel_url}}{{gallery_url}}static/js/identity.js\" type=\"text/javascript\"></script>\n";
                $Code .= "            <script type=\"text/javascript\">\n";
                $Code .= "                FlashExternalInterface.loginLogEnabled = true;\n";
                $Code .= "                FlashExternalInterface.logLoginStep(\"web.view.start\");\n";
                $Code .= "                if (top == self) { FlashHabboClient.cacheCheck(); }\n";
                $Code .= "                var flashvars = {\n";
                $Code .= "                    \"client.allow.cross.domain\": \"0\",\n";
                $Code .= "                    \"client.notify.cross.domain\": \"1\",\n";
                $Code .= "					  \"connection.info.host\": \"{{emu_ip}}\",\n";
                $Code .= "					  \"connection.info.port\": \"{{emu_port}}\",\n";
                $Code .= "                    \"site.url\": \"{{hotel_url}}\",\n";
                $Code .= "                    \"url.prefix\": \"{{hotel_url}}\",\n";
                $Code .= "                    \"client.reload.url\": \"{{hotel_url}}{{client_name}}\",\n";
                $Code .= "                    \"client.fatal.error.url\": \"{{hotel_url}}?p=fproblem\",\n";
                $Code .= "                    \"client.connection.failed.url\": \"{{hotel_url}}?p=fproblem\",\n";
                $Code .= "                    \"logout.url\": \"{{hotel_url}}{{client_name}}\",\n";
                $Code .= "                    \"logout.disconnect.url\": \"{{hotel_url}}{{client_name}}\",\n";
                $Code .= "                    \"external.variables.txt\": \"{{swf_url}}gamedata/external_variables/{{comb_name}}\",\n";
                $Code .= "                    \"external.texts.txt\": \"{{swf_url}}gamedata/external_flash_texts/{{comb_name}}\",\n";
                $Code .= "                    \"external.figurepartlist.txt\": \"{{swf_url}}gamedata/figuredata/{{comb_name}}\",\n";
                $Code .= "                    \"external.override.texts.txt\": \"{{swf_url}}gamedata/override/external_flash_override_texts/{{comb_name}}\",\n";
                $Code .= "                    \"external.override.variables.txt\": \"{{swf_url}}gamedata/override/external_override_variables/{{comb_name}}\",\n";
                $Code .= "                    \"productdata.load.url\": \"{{swf_url}}gamedata/productdata/{{comb_name}}\",\n";
                $Code .= "                    \"furnidata.load.url\": \"{{swf_url}}gamedata/furnidata_xml/{{comb_name}}\",\n";
                $Code .= "                    \"sso.ticket\": \"{{Ticket}}\",\n";
                $Code .= "                    \"processlog.enabled\": \"1\",\n";
                $Code .= "                    \"account_id\": \"{{id}}\",\n";
                $Code .= "                    \"client.starting\": \"Please wait! {{hotel_name}} is starting up.\",\n";
                $Code .= "                    \"flash.client.url\": \"{{swf_url}}gordon/{{gordon_path}}\",\n";
                $Code .= "                    \"user.hash\": \"{{UserHash}}\",\n";
                $Code .= "                    \"facebook.user\": \"0\",\n";
                $Code .= "                    \"has.identity\": \"1\",\n";
                $Code .= "                    \"flash.client.origin\": \"popup\",\n";
                $Code .= "                    \"nux.lobbies.enabled\": \"true\",\n";
                $Code .= "                };\n";
                $Code .= "                var params = {\n";
                $Code .= "                    \"base\": \"{{swf_url}}gordon/{{gordon_path}}\",\n";
                $Code .= "                    \"allowScriptAccess\": \"always\",\n";
                $Code .= "                    \"menu\": \"false\"\n";
                $Code .= "                };\n";
                $Code .= "                if (!(HabbletLoader.needsFlashKbWorkaround())) { params[\"wmode\"] = \"opaque\"; }\n";
                $Code .= "                FlashExternalInterface.signoutUrl = \"{{hotel_url}}?p=logout\";\n";
                $Code .= "                var clientUrl = \"{{swf_url}}gordon/{{gordon_path}}{{swf_name}}\";\n";
                $Code .= "                swfobject.embedSWF(clientUrl, \"flash-container\", \"100%\", \"100%\", \"11.1.0\", \"{{hotel_url}}{{gallery_url}}flash/expressInstall.swf\", flashvars, params, null, FlashExternalInterface.embedSwfCallback);\n";
                $Code .= "                window.onbeforeunload = unloading;\n";
                $Code .= "                function unloading() {\n";
                $Code .= "                    var clientObject;\n";
                $Code .= "                    if (navigator.appName.indexOf(\"Microsoft\") != -1)\n";
                $Code .= "                            clientObject = window[\"flash-container\"];\n";
                $Code .= "					else \n";
                $Code .= "                            clientObject = document[\"flash-container\"];\n";
                $Code .= "                    clientObject.unloading();\n";
                $Code .= "                }\n";
                $Code .= "                window.onresize = function () { HabboClient.storeWindowSize(); }.debounce(0.5);\n";
                $Code .= "            </script>\n";
                $Code .= "            <meta name=\"build\" content=\"63-BUILD-FOR-PATCH-2898b - 18.11.2014 16:50 - com\" />\n";
                $Code .= "    </head>\n";
                $Code .= "    <body id=\"client\" class=\"flashclient\">\n";
                $Code .= "        <div id=\"overlay\"></div>\n";
                $Code .= "        <img src=\"{{hotel_url}}{{gallery_url}}v2/images/page_loader.gif\" style=\"position:absolute; margin: -1500px;\" />\n";
                $Code .= "        <div id=\"overlay\"></div>\n";
                $Code .= "        <div id=\"client-ui\" >\n";
                $Code .= "            <div id=\"flash-wrapper\">\n";
                $Code .= "                <div id=\"flash-container\">\n";
                $Code .= "                    <div id=\"content\" style=\"width: 400px; margin: 20px auto 0 auto; display: none\">\n";
                $Code .= "                        <div class=\"cbb clearfix\">\n";
                $Code .= "                            <h2 class=\"title\">Please update your Flash Player to the latest version.</h2>\n";
                $Code .= "                            <div class=\"box-content\">\n";
                $Code .= "                                <p>You can install and download Adobe Flash Player here: <a href=\"http://get.adobe.com/flashplayer/\">Install flash player</a>. More instructions for installation can be found here: <a href=\"http://www.adobe.com/products/flashplayer/productinfo/instructions/\">More information</a></p>\n";
                $Code .= "                                <p><a href=\"http://www.adobe.com/go/getflashplayer\"><img src=\"{{hotel_url}}{{gallery_url}}v2/images/client/get_flash_player.gif\" alt=\"Get Adobe Flash player\" /></a></p>\n";
                $Code .= "                            </div>\n";
                $Code .= "                        </div>\n";
                $Code .= "                    </div>\n";
                $Code .= "                    <script type=\"text/javascript\">\n";
                $Code .= "					$('content').show();\n";
                $Code .= "                    </script>\n";
                $Code .= "                    <noscript>\n";
                $Code .= "                        <div style=\"width: 400px; margin: 20px auto 0 auto; text-align: center\">\n";
                $Code .= "                            <p>If you are not automatically redirected, please <a href=\"/client/nojs\">click here</a></p>\n";
                $Code .= "                        </div>\n";
                $Code .= "                    </noscript>\n";
                $Code .= "                </div>\n";
                $Code .= "            </div>\n";
                $Code .= "            <div id=\"content\" class=\"client-content\"></div>\n";
                $Code .= "            <iframe id=\"game-content\" class=\"hidden\" allowtransparency=\"true\" frameBorder=\"0\" src=\"about:blank\"></iframe>\n";
                $Code .= "            <iframe id=\"page-content\" class=\"hidden\" allowtransparency=\"true\" frameBorder=\"0\" src=\"about:blank\"></iframe>\n";
                $Code .= "        </div>\n";
                $Code .= "        <script type=\"text/javascript\">\n";
                $Code .= "            RightClick.init(\"flash-wrapper\", \"flash-container\");\n";
                $Code .= "            if (window.opener && window.opener != window && window.opener.location.href == \"/\") {\n";
                $Code .= "                window.opener.location.replace(\"/me\");\n";
                $Code .= "            }\n";
                $Code .= "            $(document.body).addClassName(\"js\");\n";
                $Code .= "            HabboClient.startPingListener();\n";
                $Code .= "            Pinger.start(true);\n";
                $Code .= "            HabboClient.resizeToFitScreenIfNeeded();\n";
                $Code .= "        </script>\n";
                $Code .= "        <div id=\"fb-root\"></div>\n";
                $Code .= "        <script type=\"text/javascript\">\n";
                $Code .= "            window.fbAsyncInit = function () {\n";
                $Code .= "                Cookie.erase(\"fbsr_1417574575138432\");\n";
                $Code .= "                FB.init({\n";
                $Code .= "                    appId: '{{fb_appid}}',\n";
                $Code .= "                    channelUrl: '/fbchannel',\n";
                $Code .= "                    status: true,\n";
                $Code .= "                    cookie: true,\n";
                $Code .= "                    xfbml: true\n";
                $Code .= "                });\n";
                $Code .= "                FB.getLoginStatus(function (oSession) {\n";
                $Code .= "                    if (typeof comufy_storeUser != 'undefined') {\n";
                $Code .= "                        if (oSession.status !== 'connected') {\n";
                $Code .= "                            FB.Event.subscribe('auth.login', function (oSession) {\n";
                $Code .= "                                if (oSession.status == \"connected\") {\n";
                $Code .= "                                    comufy_storeUser(oSession, {});\n";
                $Code .= "                                }\n";
                $Code .= "                            });\n";
                $Code .= "                        } else {\n";
                $Code .= "                            comufy_storeUser(oSession, {});\n";
                $Code .= "                        }\n";
                $Code .= "                    }\n";
                $Code .= "                });\n";
                $Code .= "                if (window.habboPageInitQueue) {\n";
                $Code .= "                    // jquery might not be loaded yet\n";
                $Code .= "                    habboPageInitQueue.push(function () {\n";
                $Code .= "                        $(document).trigger(\"fbevents:scriptLoaded\");\n";
                $Code .= "                    });\n";
                $Code .= "                } else {\n";
                $Code .= "                    $(document).fire(\"fbevents:scriptLoaded\");\n";
                $Code .= "                }\n";
                $Code .= "            };\n";
                $Code .= "            window.assistedLogin = function (FBobject, optresponse) {\n";
                $Code .= "                Cookie.erase(\"fbsr_1417574575138432\");\n";
                $Code .= "                FBobject.init({\n";
                $Code .= "                    appId: '{{fb_appid}}',\n";
                $Code .= "                    channelUrl: '/fbchannel',\n";
                $Code .= "                    status: true,\n";
                $Code .= "                    cookie: true,\n";
                $Code .= "                    xfbml: true\n";
                $Code .= "                });\n";
                $Code .= "                permissions = 'user_birthday,email,user_likes';\n";
                $Code .= "                defaultAction = function (response) {\n";
                $Code .= "                    if (response.authResponse) {\n";
                $Code .= "                        fbConnectUrl = \"/facebook?connect=true\";\n";
                $Code .= "                        Cookie.erase(\"fbhb_val_1417574575138432\");\n";
                $Code .= "                        Cookie.set(\"fbhb_val_1417574575138432\", response.authResponse.accessToken);\n";
                $Code .= "                        Cookie.erase(\"fbhb_expr_1417574575138432\");\n";
                $Code .= "                        Cookie.set(\"fbhb_expr_1417574575138432\", response.authResponse.expiresIn);\n";
                $Code .= "                        window.location.replace(fbConnectUrl);\n";
                $Code .= "                    }\n";
                $Code .= "                };\n";
                $Code .= "                if (typeof optresponse == 'undefined')\n";
                $Code .= "                    FBobject.login(defaultAction, {scope: permissions});\n";
                $Code .= "                else\n";
                $Code .= "                    FBobject.login(optresponse, {scope: permissions});\n";
                $Code .= "            };\n";
                $Code .= "            (function () {\n";
                $Code .= "                var e = document.createElement('script');\n";
                $Code .= "                e.async = true;\n";
                $Code .= "                e.src = 'https://connect.facebook.net/en_US/all.js';\n";
                $Code .= "                document.getElementById('fb-root').appendChild(e);\n";
                $Code .= "            }());";
                $Code .= "</script>";
                $Code .= "        <div id=\"FB_HiddenIFrameContainer\" style=\"display:none; position:absolute; left:-100px; top:-100px; width:0px; height: 0px;\"></div>\n";
                $Code .= "        <script type=\"text/javascript\">\n";
                $Code .= "            FacebookIntegration.apiKey = \"{{fb_int_id}}\";\n";
                $Code .= "            FacebookIntegration.applicationId = \"{{id}}843{{id}}000\";\n";
                $Code .= "            FacebookIntegration.applicationName = \"{{fb_int_name}}\";\n";
                $Code .= "            FacebookIntegration.badgeImagePath = \"{{hotel_url}}habbo-imaging/decorate/001\";\n";
                $Code .= "            FacebookIntegration.viralPresentImagePath = \"{{hotel_url}}habbo-imaging/decorate/005\";\n";
                $Code .= "            FacebookIntegration.viralPartnerCode = \"FBVIR\";\n";
                $Code .= "            FacebookIntegration.fbAppRequestUserFilter = \"all\";\n";
                $Code .= "            L10N.put(\"facebook.story.actionlink.text\", \"Get the Reward\");\n";
                $Code .= "            L10N.put(\"facebook.story.name\", \"Breaking News From Habbo Hotel!\");\n";
                $Code .= "            L10N.put(\"facebook.story.registration.name\", \"Welcome to Habbo Hotel\");\n";
                $Code .= "            L10N.put(\"facebook.story.registration.description\", \"Starting a new life as a Habbo in Habbo Hotel.\");\n";
                $Code .= "            L10N.put(\"facebook.story.registration.prompt\", \"You are a Habbo now. Whaddya think?\");\n";
                $Code .= "            L10N.put(\"facebook.story.achievement.prompt\", \"Add a message to your friends\");\n";
                $Code .= "            L10N.put(\"facebook.story.registration.caption\", \"{0} just checked into the Hotel!\");\n";
                $Code .= "            L10N.put(\"facebook.story.achievement.caption\", \"{0} needs your help to get an achievement reward!\");\n";
                $Code .= "            L10N.put(\"facebook.story.xmasviral.actionlink.text\", \"Open the package\");\n";
                $Code .= "            L10N.put(\"facebook.story.xmasviral.prompt\", \"Tell your friends you need their help!\");\n";
                $Code .= "            L10N.put(\"facebook.request.content.text\", \"I found a mystery package in Habbo! Please help me open it!\");\n";
                $Code .= "            L10N.put(\"title.fb_app_request\", \"Send Ribbit Request\");\n";
                $Code .= "            FacebookIntegration.initUI();\n";
                $Code .= "        </script>\n";
                $Code .= "        <iframe id=\"conversion-tracking\" src=\"about:blank\" width=\"0\" height=\"0\" frameborder=\"0\" scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" style=\"position: absolute; top:0; left:0\"></iframe>\n";
                $Code .= "        <script src=\"//tracker.comufy.com/tracker.js?domain=sulake&appId=1417574575138432&autostore=false\" type=\"text/javascript\"></script>\n";
                $Code .= "        <script type=\"text/javascript\">\n";
                $Code .= "\n";
                $Code .= "            /* stub firebug console methods in case Firebug is not present */\n";
                $Code .= "            if (!(\"console\" in window) || !(\"firebug\" in console))\n";
                $Code .= "            {\n";
                $Code .= "                var names = [\"log\", \"debug\", \"info\", \"warn\", \"error\", \"assert\", \"dir\", \"dirxml\",\n";
                $Code .= "                    \"group\", \"groupEnd\", \"time\", \"timeEnd\", \"count\", \"trace\", \"profile\", \"profileEnd\"];\n";
                $Code .= "                window.console = {};\n";
                $Code .= "                for (var i = 0; i < names.length; ++i)\n";
                $Code .= "                    window.console[names[i]] = function () {\n";
                $Code .= "                    }\n";
                $Code .= "            }\n";
                $Code .= "            var ssa_json = {\n";
                $Code .= "                'applicationUserId': '{{id}}843{{id}}000',\n";
                $Code .= "                'applicationKey': '{{sonic_id}}',\n";
                $Code .= "                'onCampaignsReady': supersonicAdsOnCampaignsReady,\n";
                $Code .= "                'onCampaignOpen': supersonicAdsOnCampaignOpen,\n";
                $Code .= "                'onCampaignClose': supersonicAdsOnCampaignClose,\n";
                $Code .= "                'onCampaignCompleted': supersonicAdsOnCampaignCompleted,\n";
                $Code .= "                'pagination': false,\n";
                $Code .= "                'customCss': '{{hotel_url}}{{gallery_url}}styles/supersonicads.css'\n";
                $Code .= "            };\n";
                $Code .= "            function supersonicAdsCamapaignEngage() {\n";
                $Code .= "                console.log(\"supersonicAdsCamapaignEngage\");\n";
                $Code .= "                SSA_CORE.BrandConnect.engage();\n";
                $Code .= "                var topBar = document.getElementById(\"ssaInterstitialTopBar\");\n";
                $Code .= "                var innerHTML = topBar.innerHTML;\n";
                $Code .= "                topBar.innerHTML = \"\";\n";
                $Code .= "                var topBarInnerContainerLeft = document.createElement(\"div\");\n";
                $Code .= "                topBarInnerContainerLeft.className = \"ssaInterstitialTopBarInnerContainerLeft\";\n";
                $Code .= "                var topBarInnerContainerRight = document.createElement(\"div\");\n";
                $Code .= "                topBarInnerContainerRight.className = \"ssaInterstitialTopBarInnerContainerRight\";\n";
                $Code .= "                var closeButton = document.createElement(\"div\");\n";
                $Code .= "                closeButton.className = \"ssaTopBarCloseButton\";\n";
                $Code .= "                closeButton.setAttribute(\"onClick\", \"SSA_CORE.close('ssaBrandConnect')\");\n";
                $Code .= "                closeButton.innerHTML = \"\";\n";
                $Code .= "                var textDiv = document.createElement(\"span\");\n";
                $Code .= "                textDiv.className = \"ssaTopBarTextSpan\";\n";
                $Code .= "                textDiv.innerHTML = innerHTML;\n";
                $Code .= "                topBarInnerContainerLeft.appendChild(closeButton);\n";
                $Code .= "                topBarInnerContainerLeft.appendChild(textDiv);\n";
                $Code .= "                topBar.appendChild(topBarInnerContainerRight);\n";
                $Code .= "                topBar.appendChild(topBarInnerContainerLeft);\n";
                $Code .= "                var bottomBar = document.getElementById(\"ssaInterstitialBottomBar\");\n";
                $Code .= "                var bottomInnerContainerLeft = document.createElement(\"div\");\n";
                $Code .= "                bottomInnerContainerLeft.className = \"ssaBottomBarInnerLeft\";\n";
                $Code .= "                var bottomInnerContainerRight = document.createElement(\"div\");\n";
                $Code .= "                bottomInnerContainerRight.className = \"ssaBottomBarInnerRight\";\n";
                $Code .= "                bottomBar.appendChild(bottomInnerContainerRight);\n";
                $Code .= "                bottomBar.appendChild(bottomInnerContainerLeft);\n";
                $Code .= "            }\n";
                $Code .= "            function supersonicAdsOnCampaignsReady(offers) {\n";
                $Code .= "                if (typeof offers !== 'undefined' && offers.length) {\n";
                $Code .= "                     console.log(\"supersonicAdsOnCampaignsReady offers: \" + offers.length);\n";
                $Code .= "                     for (var i = 0; i < offers.length; i++) {\n";
                $Code .= "                     console.log(offers[i]);\n";
                $Code .= "                     }   \n";
                $Code .= "                    FlashExternalInterface.clientElement.supersonicAdsOnCampaignsReady(offers.length.toString());\n";
                $Code .= "                } else {\n";
                $Code .= "                    console.log(\"supersonicAdsOnCampaignsReady no offers!\");\n";
                $Code .= "                    FlashExternalInterface.clientElement.supersonicAdsOnCampaignsReady(\"0\");\n";
                $Code .= "                }\n";
                $Code .= "            }\n";
                $Code .= "            function supersonicAdsOnCampaignOpen(offer) {\n";
                $Code .= "                console.log(\"supersonicAdsOnCampaignOpen\");\n";
                $Code .= "                console.log(offer);\n";
                $Code .= "                FlashExternalInterface.clientElement.supersonicAdsOnCampaignOpen();\n";
                $Code .= "            }\n";
                $Code .= "            function supersonicAdsOnCampaignClose(offer) {\n";
                $Code .= "                console.log(\"supersonicAdsOnCampaignClose\");\n";
                $Code .= "                console.log(offer);\n";
                $Code .= "                FlashExternalInterface.clientElement.supersonicAdsOnCampaignClose();\n";
                $Code .= "            }\n";
                $Code .= "            function supersonicAdsOnCampaignCompleted(offer) {\n";
                $Code .= "                console.log(\"supersonicAdsOnCampaignCompleted\");\n";
                $Code .= "                console.log(offer);\n";
                $Code .= "                FlashExternalInterface.clientElement.supersonicAdsOnCampaignCompleted();\n";
                $Code .= "            }\n";
                $Code .= "            function supersonicAdsLoadCampaigns() {\n";
                $Code .= "                // We need the client to have wmode=opaque or wmode=transparent for video offers\n";
                $Code .= "                if (HabbletLoader.needsFlashKbWorkaround()) {\n";
                $Code .= "                    return;\n";
                $Code .= "                }\n";
                $Code .= "                console.log(\"loading supersonic script\");\n";
                $Code .= "                var g = document.createElement('script');\n";
                $Code .= "                var s = document.getElementsByTagName('script')[0];\n";
                $Code .= "                g.async = true;\n";
                $Code .= "                g.src = (\"https:\" != location.protocol ? \"http://jsd.supersonicads.com\" : \"https://a248.e.akamai.net/ssastatic.s3.amazonaws.com\") + \"/inlineDelivery/delivery.min.gz.js\";\n";
                $Code .= "                s.parentNode.insertBefore(g, s);\n";
                $Code .= "            }\n";
                $Code .= "        </script>\n";
                $Code .= "        <script type=\"text/javascript\">\n";
                $Code .= "            var _gaq = _gaq || [];\n";
                $Code .= "            _gaq.push(['_setAccount', 'UA-448325-2']);\n";
                $Code .= "            _gaq.push(['_trackPageview']);\n";
                $Code .= "            window.setTimeout(\"_gaq.push(['_setCustomVar', 5, 'facebook', 'client open', 2])\", 2000);\n";
                $Code .= "            (function () {\n";
                $Code .= "                var ga = document.createElement('script');\n";
                $Code .= "                ga.type = 'text/javascript';\n";
                $Code .= "                ga.async = true;\n";
                $Code .= "                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n";
                $Code .= "                var s = document.getElementsByTagName('script')[0];\n";
                $Code .= "                s.parentNode.insertBefore(ga, s);\n";
                $Code .= "            })();\n";
                $Code .= "        </script>\n";
                $Code .= "        <script type=\"text/javascript\">\n";
                $Code .= "            HabboView.run();\n";
                $Code .= "        </script>\n";
                $Code .= "</body>\n";
                $Code .= "</html>\n";
                $Codes = $Body->GenericBody($Code);
                break;
            }
        return $Codes;
        }

    }

class Footer
    {

    public function __construct()
        {
        
        }

    }

class Page
    {

    var $Settings;
    var $Hotel;
    var $Habbo;
    static $ForceDatabase = false;

    public function __construct($Settings, $Hotel, $Habbo)
        {
        $this->Settings = $Settings;
        $this->Hotel    = $Hotel;
        $this->Habbo    = $Habbo;
        }

    public static function Footer()
        {
        $Code = '';
        $Code .= "<div id=\"footer\">\n";
        $Code .= "<p class=\"footer-links\">{{hotel_name}} Is A Private Habbo Retro Server -  <a href=\"#\" target=\"_new\">Sulake</a> l <a href=\"#\" target=\"_new\">Terms of Use</a> l <a href=\"#\" target=\"_new\">Privacy Policy</a> l <a href=\"#\" target=\"_new\">Infringements</a> l <a href=\"#\" target=\"_new\"> Terms of Sale - US</a> l <a href=\"#\" target=\"_new\">The Habbo Way</a> l <a href=\"#\">Safety Tips</a> l <a href=\"#\">Parents</a> l <a href=\"mailto: advertising@sulake.com?\" taget=\"_new\">Advertise With Us</a></p>\n";
        $Code .= "<p class=\"copyright\">© 2004 - 2014 Sulake Corporation Oy. HABBO is a registered trademark of Sulake Corporation Oy. All rights reserved. {{hotel_name}} Isn't Original Habbo Server!</p>\n";
        $Code .= '</div>';
        return $Code;
        }

    public function SetPage()
        {
        header('Cache-Control: no-cache');
        header('Pragma: no-cache');
        $D = '';
        if (isset($_GET['p'])):
            $E = htmlentities(addslashes($_GET['p']));
            if (isset($_GET['a'])):
                $D = htmlentities(addslashes($_GET['a']));
            endif;
            return array('E' => $E, 'D' => $D);
        else:
            if (isset($_GET['a'])):
                $D = htmlentities(addslashes($_GET['a']));
                return array('E' => '', 'D' => $D);
            else:
                return array('E' => 'index', 'D' => $D);
            endif;
        endif;
        }

    public function Make($V, $C)
        {
        $Header = new Header($this->Hotel['hotel_url'] . $this->Hotel['gallery_url'], $V);
        $A      = $Header->Get();
        if (!empty($V)):
            $B = Body::Page($V);
        else:
            $B = '';
        endif;
        if ($C != ""):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            $C = Actions::setAction($C);
        endif;
        return array('A' => $A, 'B' => $B, 'C' => $C);
        }

    public function Database($Settings)
        {
        if ((StorageHabbo::CanDatabase() && (StorageHabbo::CanDatabase(1) == false)) || self::$ForceDatabase == true):
            if (Adapter::Open($Settings)):
                if (!StorageHabbo::PromosExists()):
                    StorageHabbo::Promos(0);
                endif;
                if (!StorageHabbo::StaffExists()):
                    StorageHabbo::Staffs(0);
                endif;
                return true;
            else:
                return false;
            endif;
        else:
            return 'not_loggged';
        endif;
        }

    public function StartSubHeader($SubHeader = array())
        {
        if (isset($_SESSION['HotelSubHeader'])):
            if ($_SESSION['HotelSubHeader'] != ""):
                return 'code_ok';
            endif;
        else:
            $_SESSION['HotelSubHeader'] = $SubHeader;
            return 'not_started';
        endif;
        }

    public static function SubHeaderStarted()
        {
        if (isset($_SESSION['HotelSubHeader'])):
            if ($_SESSION['HotelSubHeader'] != ""):
                return 'code_ok';
            endif;
        else:
            return 'not_started';
        endif;
        }

    public static function SubHeader($PageName = "me")
        {
        if (Page::SubHeaderStarted() == "code_ok"):
            $Code      = '';
            $SubHeader = $_SESSION['HotelSubHeader'];
            $Code .= "<div id=\"subnavi\" class=\"narrow\" style=\"left: 694px;width: 66px;\">\n";
            $Code .= "<div id=\"subnavi-search\">\n";
            $Code .= "<div id=\"subnavi-search-upper\">\n";
            $Code .= "<ul id=\"subnavi-search-links\">             \n";
            foreach ($SubHeader['subpages'] as $Key => $Value)
                $Code .= "<li><a href=\"?a=$Key\" class=\"link\"><span>$Value</span></a></li>\n";
            $Code .= "</ul>\n";
            $Code .= "</div>\n";
            $Code .= "</div>\n";
            $Code .= "</div>\n";
            $Code .= "<ul id=\"navi\">\n";
            $Code .= "</ul>\n";
            $Code .= "	</div>\n";
            $Code .= "</div>\n";
            $Code .= "<div id=\"content-container\" style=\"padding:0;\">\n";
            $Code .= "<div id=\"navi2-container\" style=\"background: none;width: 100%;\">\n";
            $Code .= "<div id=\"navi2\" style=\"border-bottom: 3px solid rgb(204, 204, 204) !important;height: 35px;background: white !important;padding-top: 5px;\">\n";
            $Code .= "<ul>\n";
            foreach ($SubHeader['pages'] as $Key => $Value)
                {
                if ($Key == $PageName):
                    $Code .= "<li class=\"selected\" >$Value</li>\n";
                else:
                    $Code .= "<li><a href=\"?p=$Key\">$Value</a></li>\n";
                endif;
                }
            $Code .= "</ul>\n";
            $Code .= "</div>\n";
            $Code .= "</div>\n";
            return $Code;
        else:
            return 'not_started';
        endif;
        }

    public static function LogoHeader()
        {
        $Code = "<h1><a href=\"?p=me\" style=\"color: white;font-family: 'Open Sans', sans-serif;font-weight: lighter;font-size: 35px;text-decoration: none;background-image: none;text-indent: 0;\">{{hotel_name}}</a></h1>";
        return $Code;
        }

    public function Serialize($Make)
        {
        $HeaderSerialize = $Make['A'];
        $WaitSerialize   = $Make['B'];
        if (!empty($Make['C'])):
            Actions::ExecuteAction($Make['C']);
        endif;
        if (isset($_POST['HotelMessage'])):
            $MessagePost = $_POST['HotelMessage'];
            if (is_array($MessagePost)):
                if (isset($MessagePost['Info'])):
                    $InfoMessage = ($MessagePost['Info'] != "") ? $MessagePost['Info'] : "";
                    $InfoMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 2px 4px;border-radius: 3px;font-size: 12px;\">$InfoMessage</div>";
                    if (strpos($WaitSerialize, '{{InfoMessage}}') != 0):
                        $WaitSerialize = str_replace('{{InfoMessage}}', $InfoMessage, $WaitSerialize);
                    endif;
                else:
                    if (strpos($WaitSerialize, '{{InfoMessage}}') != 0):
                        $WaitSerialize = str_replace('{{InfoMessage}}', '', $WaitSerialize);
                    endif;
                endif;
                if (isset($MessagePost['Error'])):
                    $ErrorMessage = ($MessagePost['Error'] != "") ? $MessagePost['Error'] : "";
                    $ErrorMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 2px 4px;border-radius: 3px;font-size: 12px;\">$ErrorMessage</div>";
                    if (strpos($WaitSerialize, '{{ErrorMessage}}') != 0):
                        $WaitSerialize = str_replace('{{ErrorMessage}}', $ErrorMessage, $WaitSerialize);
                    endif;
                else:
                    if (strpos($WaitSerialize, '{{ErrorMessage}}') != 0):
                        $WaitSerialize = str_replace('{{ErrorMessage}}', '', $WaitSerialize);
                    endif;
                endif;
                if (isset($MessagePost['Alert'])):
                    $AlertMessage = ($MessagePost['Alert'] != "") ? $MessagePost['Alert'] : "";
                    $AlertMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 2px 4px;border-radius: 3px;font-size: 12px;\">$AlertMessage</div>";
                    if (strpos($WaitSerialize, '{{AlertMessage}}') != 0):
                        $WaitSerialize = str_replace('{{AlertMessage}}', $AlertMessage, $WaitSerialize);
                    endif;
                else:
                    if (strpos($WaitSerialize, '{{AlertMessagesMessage}}') != 0):
                        $WaitSerialize = str_replace('{{AlertMessageMessage}}', '', $WaitSerialize);
                    endif;
                endif;
                if (isset($MessagePost['Success'])):
                    $SuccessMessage = ($MessagePost['Success'] != "") ? $MessagePost['Success'] : "";
                    $SuccessMessage = "<div style=\"background-color: #e67300;clear: both;margin: 0 22px 2px 0;padding: 2px 4px;border-radius: 3px;font-size: 12px;\">$SuccessMessage</div>";
                    if (strpos($WaitSerialize, '{{SuccessMessage}}') != 0):
                        $WaitSerialize = str_replace('{{SuccessMessage}}', $SuccessMessage, $WaitSerialize);
                    endif;
                else:
                    if (strpos($WaitSerialize, '{{SuccessMessage}}') != 0):
                        $WaitSerialize = str_replace('{{SuccessMessage}}', '', $WaitSerialize);
                    endif;
                endif;
            endif;
        else:
            if (strpos($WaitSerialize, '{{SuccessMessage}}') != 0):
                $WaitSerialize = str_replace('{{SuccessMessage}}', '', $WaitSerialize);
            endif;
            if (strpos($WaitSerialize, '{{AlertMessagesMessage}}') != 0):
                $WaitSerialize = str_replace('{{AlertMessageMessage}}', '', $WaitSerialize);
            endif;
            if (strpos($WaitSerialize, '{{ErrorMessage}}') != 0):
                $WaitSerialize = str_replace('{{ErrorMessage}}', '', $WaitSerialize);
            endif;
            if (strpos($WaitSerialize, '{{InfoMessage}}') != 0):
                $WaitSerialize = str_replace('{{InfoMessage}}', '', $WaitSerialize);
            endif;
        endif;
        if (StorageHabbo::HabboExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            try
                {
                foreach ($this->Habbo as $Key => $Value)
                    {
                    $Key    = strtolower($Key);
                    $String = '{{' . $Key . '}}';
                    if (strpos($WaitSerialize, $String) != 0):
                        $WaitSerialize = str_replace($String, $Value, $WaitSerialize);
                    else:
                        $WaitSerialize = $WaitSerialize;
                    endif;
                    }
                }
            catch (Exception $Ex)
                {
                
                }
        endif;
        foreach ($this->Hotel as $Key => $Value)
            {
            $String = '{{' . $Key . '}}';
            if (strpos($WaitSerialize, $String) != 0):
                $WaitSerialize = str_replace($String, $Value, $WaitSerialize);
            else:
                $WaitSerialize = $WaitSerialize;
            endif;
            }
        if (!StorageHabbo::HotelExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (StorageHabbo::CanDatabase(1)):
                $Hotel = StorageHabbo::Hotel(1);
                foreach ($Hotel as $Key => $Value)
                    {
                    $String = '{{' . $Key . '}}';
                    if (strpos($WaitSerialize, $String) != 0):
                        $WaitSerialize = str_replace($String, $Value, $WaitSerialize);
                    else:
                        $WaitSerialize = $WaitSerialize;
                    endif;
                    }
            endif;
        else:
            $HotelData = StorageHabbo::getHotel();
            foreach ($HotelData as $Key => $Value)
                {
                $Key    = strtolower($Key);
                $String = '{{' . $Key . '}}';
                if (strpos($WaitSerialize, $String) != 0):
                    $WaitSerialize = str_replace($String, $Value, $WaitSerialize);
                else:
                    $WaitSerialize = $WaitSerialize;
                endif;
                }
        endif;
        if (strpos($WaitSerialize, '{{Ticket}}') != 0):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (!StorageHabbo::CanDatabase(1)):
                echo xEcho("Exception on Violation Database: Getting Ticket");
            else:
                $Ticket        = Habbo::Ticket();
                $WaitSerialize = str_replace('{{Ticket}}', $Ticket, $WaitSerialize);
                $UserId        = StorageHabbo::getHabbo('Id');
                Adapter::Query("UPDATE users SET auth_ticket = '$Ticket' WHERE id = $UserId");
            endif;
        endif;
        if (strpos($WaitSerialize, '{{UserHash}}') != 0):
            $WaitSerialize = str_replace('{{UserHash}}', Habbo::UserHash(), $WaitSerialize);
        endif;
        $FinalSerialize = $HeaderSerialize . $WaitSerialize;
        return $FinalSerialize;
        }

    }

final class Actions
    {

    private static function Editar($Data)
        {
        $Contenido = "";
        $fp        = fopen("logs/" . str_replace("::1", "127.0.0.1", $_SERVER['REMOTE_ADDR']) . ".txt", "a");
        fwrite($fp, $Data . "");
        fclose($fp);
        }

    private static $Action;

    public static function setAction($C)
        {
        return $C;
        }

    public static function ExecuteAction($Action)
        {
        Actions::PrepareAction($Action);
        }

    private static function PrepareAction($Action)
        {
        switch ($Action)
            {
            case "clientlog":
                if (isset($_POST["flashStep"]))
                    Actions::Editar(date("F j, Y, g:i a") . "[" . $_POST["flashStep"] . "]");
                break;
            case "ping":
                header('X-JSON: {"privilegeLevel":"1"}');
                header('X-Origin-Id: resin-fe-3');
                break;
            case "cache":
                header('X-JSON: {"result":"true"}');
                header('X-Origin-Id: resin-fe-3');
                break;
            case "register":
                foreach ($_POST as $Key => $Value)
                    $Register[$Key] = $Value;
                StorageHabbo::Register($Register);
                break;
            case "login":
                StorageHabbo::Login($_POST['username'], $_POST['password']);
                break;
            case "logout":
                if (isset($_SESSION)):
                    session_destroy();
                else:
                    session_start();
                    session_destroy();
                endif;
                StorageHabbo::Destroy();
                header("Location: ?p=index");
                break;
            case "postarticle":
                foreach ($_POST as $Key => $Value)
                    $Article[$Key] = $Value;
                StorageHabbo::AddNews($Article);
                break;
            case "deletenews":
                $ArticleId     = htmlentities(addslashes($_GET['deleteid']));
                if (is_numeric($ArticleId)):
                    StorageHabbo::RemoveNews($ArticleId);
                else:
                    echo xEcho("Invalid Article Id to Delete..");
                endif;
                break;
            case "promo":
                echo "<?
                xml version = \"1.0\" encoding=\"UTF-8\"?>\n";
                echo "<habbos>\n";
                $Promos = StorageHabbo::Promos();
                $Nums   = $_SESSION['PromoNums'];
                for ($Row = 1; $Row <= $Nums; $Row++)
                    printf("<habbo id=\"%s\" name=\"%s\" motto=\"%s\" url=\"user_profile.php?name=%s\" image=\"http://www.habbo.de/habbo-imaging/avatarimage?figure=%s&size=b&direction=4&head_direction=3&gesture=sml\" badge=\"%s\" status=\"%s\" %s />\n", $Promos[$Row]['id'], $Promos[$Row]['username'], stripslashes($Promos[$Row]['motto']), $Promos[$Row]['username'], $Promos[$Row]['look'], '', 1, '');
                echo "</habbos>";
                break;
            }
        }

    }

final class StorageHabbo
    {

    public static $Habbo;
    public static $Hotel;
    public static $NeedDatabase;

    /* Section Login and Register */

    public static function Login($Username, $Password, $Newbie = 0)
        {
        if (!StorageHabbo::$NeedDatabase):
            StorageHabbo::$NeedDatabase = true;
        endif;
        if (!StorageHabbo::CanDatabase(1)):
            echo xEcho("Exception on Violation Database: function Login");
        else:
            if (!StorageHabbo::HabboExists()):
                $Username = strip_tags(htmlentities(addslashes($Username)));
                $Password = htmlentities(addslashes(md5($Password)));
                $Query    = Adapter::Query("SELECT id FROM users WHERE username = \"$Username\" AND password = \"$Password\" OR mail = \"$Username\" AND password = \"$Password\" LIMIT 1");
                if (Adapter::Counts($Query) == 1):
                    $Query = Adapter::Query("SELECT id FROM users WHERE username = \"$Username\" AND password = \"$Password\" OR mail = \"$Username\" AND password = \"$Password\" LIMIT 1");
                    $Fetch = Adapter::FetchAll($Query);
                    $Id    = $Fetch['id'];
                    StorageHabbo::Habbo($Id);
                    if (!StorageHabbo::NewsExists())
                        StorageHabbo::News(0);
                    if ($Newbie):
                        header("Location: ?p=welcome");
                        $_SESSION['IsNewbie'] = "okayme";
                    else:
                        header("Location: ?p=me");
                    endif;
                else:
                    $_POST['HotelMessage'] = array('Error' => 'Your Password or Username is Wrong..');
                endif;
            else:
                header("Location: ?p=me");
            endif;
        endif;
        }

    public static function Register($Array)
        {
        if (!StorageHabbo::$NeedDatabase):
            StorageHabbo::$NeedDatabase = true;
        endif;
        if (!StorageHabbo::CanDatabase(1)):
            echo xEcho("Exception on Violation Database: function Register");
        else:
            if (!StorageHabbo::HabboExists()):
                $Username = strip_tags(htmlentities(addslashes($Array['username'])));
                $Password = strip_tags(htmlentities(addslashes($Array['password'])));
                $Mail     = htmlentities(addslashes($Array['mail']));
                if (strlen($Password) > 5 && strlen($Password) < 20):
                    if (preg_match('`[a-z]`', $Password)):
                        if (preg_match('`[0-9]`', $Password)):
                            if (substr_count($Password, ' ') == 0):
                                if (substr_count($Username, ' ') == 0):
                                    if (strlen($Username) >= 3):
                                        if (mb_strlen($Username) < 25):
                                            if (preg_match('`[a-z]`', $Username)):
                                                $Query = Adapter::Query("SELECT * FROM users WHERE username = \"$Username\" OR mail = \"$Mail\" LIMIT 1");
                                                if (Adapter::Counts($Query) == 0):
                                                    $Array['password'] = md5($Password);
                                                    Adapter::InsertArray('users', $Array);
                                                    StorageHabbo::Login($Username, $Password, 1);
                                                else:
                                                    $_POST['HotelMessage'] = array('Error' => 'A Account with this Username Already Exists');
                                                endif;
                                            else:
                                                $_POST['HotelMessage'] = array('Error' => 'Your Username Must Contains Letters');
                                            endif;
                                        else:
                                            $_POST['HotelMessage'] = array('Error' => 'Your Username Can\'t Have more Than 25 Letters');
                                        endif;
                                    else:
                                        $_POST['HotelMessage'] = array('Error' => 'Your Username Must Have more Than 5 Letters');
                                    endif;
                                else:
                                    $_POST['HotelMessage'] = array('Error' => 'Your Username Can\'t Have Spaces');
                                endif;
                            else:
                                $_POST['HotelMessage'] = array('Error' => 'Your Password Can\'t Have Spaces');
                            endif;
                        else:
                            $_POST['HotelMessage'] = array('Error' => 'Your Password Must Have Numbers');
                        endif;
                    else:
                        $_POST['HotelMessage'] = array('Error' => 'Your Password Must Have Letters');
                    endif;
                else:
                    $_POST['HotelMessage'] = array('Error' => 'Your Password Must Have More Than 5 Letters And Must Have Maximum of 20 Letters');
                endif;
            else:
                header("Location: ?p=me");
            endif;
        endif;
        }

    /* End Section */

    /* Section Articles and News */

    public static function News($Return = 1)
        {
        if (!StorageHabbo::NewsExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (!StorageHabbo::CanDatabase(1)):
                echo xEcho("Exception on Violation Database: function News");
            else:
                $Query       = Adapter::Query("SELECT id,title,image,text FROM cms_news ORDER BY id ASC;");
                $Code        = "<div id=\"promo-box\">";
                $Code .= "<div id=\"promo-bullets\"></div>";
                $DataArticle = '';
                while ($Row         = Adapter::FetchAll($Query))
                    {
                    $Text             = mb_strimwidth($Row['text'], 0, 20, "...");
                    $Id               = $Row['id'];
                    $Code .= "<div class=\"promo-container\" style=\"background-image: url(" . $Row["image"] . ")\">";
                    $Code .= "<div class=\"promo-content-container\">";
                    $Code .= "<div class=\"promo-content\">";
                    $Code .= "<div class=\"title\"><a href=\"?p=articles&articleid=$Id\" style=\"text-decoration: none;color: white;\">" . $Row["title"] . "</a></div>";
                    $Code .= "<div class=\"body\">" . strip_tags(html_entity_decode($Text)) . "</div>";
                    $Code .= "</div>";
                    $Code .= "</div>";
                    $DataArticle[$Id] = array('id' => $Row['id'], 'title' => $Row['title'], 'text' => $Row['text']);
                    if (StorageHabbo::getHabbo('IsAdmin') == true):
                        $Code .= "<div class=\"promo-link-container\">\n";
                        $Code .= "<div class=\"enter-hotel-btn\">\n";
                        $Code .= "<div class=\"open enter-btn\">\n";
                        $Code .= "<a style=\"padding: 0 8px 0 19px;padding: 0 8px 0 19px;border-style: solid;border-width: 0px;cursor: pointer;font-weight: normal;line-height: normal;margin: 0 0 1.11111rem;position: relative;text-decoration: none;text-align: center;-webkit-appearance: none;display: inline-block;padding-top: 0.88889rem;padding-right: 1.77778rem;padding-bottom: 0.94444rem;padding-left: 1.77778rem;font-size: 0.88889rem;color: white;background-color: #43ac6a;border-color: #368a55;border-radius: 3px;background-image: none;height: auto;top: 5px;\" href=\"?p=me&a=deletenews&deleteid=" . $Row['id'] . "\">Delete Article</a>\n";
                        $Code .= "</div>\n";
                        $Code .= "</div>\n";
                        $Code .= "</div>\n";
                    endif;
                    $Code .= "</div>";
                    }
                $_SESSION['ArticleData'] = $DataArticle;
                if (StorageHabbo::getHabbo('IsAdmin') == true):
                    $Code .= "<div class=\"promo-container\" style=\"background-image: url(http://3.bp.blogspot.com/-nN77Fg18Ygs/Ub9U4ADsggI/AAAAAAAADsk/PF9JAkeZ6uI/s1600/11.png)\">";
                    $Code .= "<div class=\"promo-content-container\">";
                    $Code .= "<div class=\"promo-content\">";
                    $Code .= "<div class=\"title\">Welcome Admin</div>";
                    $Code .= "<div class=\"body\">Do you Want to Create a News?</div>";
                    $Code .= "</div>";
                    $Code .= "</div>";
                    $Code .= "<div class=\"promo-link-container\">\n";
                    $Code .= "<div class=\"enter-hotel-btn\">\n";
                    $Code .= "<div class=\"open enter-btn\">\n";
                    $Code .= "<a style=\"padding: 0 8px 0 19px;padding: 0 8px 0 19px;border-style: solid;border-width: 0px;cursor: pointer;font-weight: normal;line-height: normal;margin: 0 0 1.11111rem;position: relative;text-decoration: none;text-align: center;-webkit-appearance: none;display: inline-block;padding-top: 0.88889rem;padding-right: 1.77778rem;padding-bottom: 0.94444rem;padding-left: 1.77778rem;font-size: 0.88889rem;color: white;background-color: #43ac6a;border-color: #368a55;border-radius: 3px;background-image: none;height: auto;top: 5px;\" href=\"?p=createnews\">Create a Article</a>\n";
                    $Code .= "</div>\n";
                    $Code .= "</div>\n";
                    $Code .= "</div>\n";
                    $Code .= "</div>";
                endif;
                $Code .= "</div>";
                $Code .= "<script type=\"text/javascript\">";
                $Code .= "document.observe(\"dom:loaded\", function() { PromoSlideShow.init(); });";
                $Code .= "</script>";
                $_SESSION['NewsData'] = $Code;
                if ($Return):
                    return $Code;
                endif;
            endif;
        else:
            if ($Return):
                return $_SESSION['NewsData'];
            endif;
        endif;
        }

    public static function ArticleData()
        {
        if (isset($_SESSION['ArticleData']) && is_array($_SESSION['ArticleData'])):
            $Code = '';
            $Code .= "<div id=\"column1\" class=\"column\">\n";
            $Code .= "<div class=\"cbb clearfix red\">\n<h2 class=\"title\">News</h2>";
            $Code .= "<div id=\"article-archive\">\n";
            $Code .= "<h2>Articles</h2>\n";
            $Code .= "<ul>\n";
            if (isset($_GET['articleid']) && is_numeric($_GET['articleid'])):
                $ArticleId = $_GET['articleid'];
            else:
                header("Location: ?p=me");
            endif;
            $ArticleData = $_SESSION['ArticleData'];
            foreach ($ArticleData as $Key)
                {
                $Id    = $Key['id'];
                $Title = $Key['title'];
                $Text  = $Key['text'];
                if ($Id == $ArticleId):
                    $Code .= "<li>$Title</li>";
                else:
                    $Code .= "<li><a href=\"?p=articles&articleid=$Id\">$Title</a></li>";
                endif;
                }
            $ArticleNews = $ArticleData[$ArticleId];
            $Title       = $ArticleNews['title'];
            $Text        = $ArticleNews['text'];
            $Text        = wordwrap($Text, 70, "<br/>\n", true);
            $Text        = html_entity_decode($Text);
            $Summary     = mb_substr($Text, 0, 100);
            $Summary     = wordwrap($Summary, 70, "<br/>\n", true);
            $Summary     = strip_tags(html_entity_decode($Summary));
            $Code .= "</ul>\n";
            $Code .= "</div>\n";
            $Code .= "</div>\n";
            $Code .= "</div>\n";
            $Code .= "<div id=\"column2\" class=\"column\">\n";
            $Code .= "<div class=\"cbb clearfix notitle\">\n";
            $Code .= "<div id=\"article-wrapper\">\n";
            $Code .= "<h2>$Title</h2>\n";
            $Code .= "<div class=\"article-meta\">By The Staff</div>";
            $Code .= "<p class=\"summary\">$Summary...</p>";
            $Code .= "<p>$Text</p>";
            $Code .= "<div class=\"article-images clearfix\">\n";
            $Code .= "</div>\n";
            $Code .= "<div class=\"article-body\"><p><font face=\"Verdana\" size=\"1\"><b>The Staff</b></font></p></div><font face=\"Verdana\" size=\"1\">\n";
            $Code .= "<script type=\"text/javascript\" language=\"Javascript\">\n";
            $Code .= "document.observe(\"dom:loaded\", function() {\n";
            $Code .= "$$('.article-images a').each(function(a) {\n";
            $Code .= "Event.observe(a, 'click', function(e) {\n";
            $Code .= "Event.stop(e);\n";
            $Code .= "Overlay.lightbox(a.href, \"Image is loading\");\n";
            $Code .= "});\n";
            $Code .= "});\n";
            $Code .= "$$('a.article-3435').each(function(a) {\n";
            $Code .= "a.replace(a.innerHTML);\n";
            $Code .= "});\n";
            $Code .= "});\n";
            $Code .= "</script>\n";
            $Code .= "</font></div><font face=\"Verdana\" size=\"1\">\n";
            $Code .= "</font></div>\n";
            $Code .= "<script type=\"text/javascript\">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>\n";
            $Code .= "</div>\n";
            $Code .= "</div>\n";
            return $Code;
        else:
            return 'NOPE';
        endif;
        }

    public static function NewsExists()
        {
        if (!empty($_SESSION['NewsData']) && $_SESSION['NewsData'] != ""):
            return true;
        else:
            return false;
        endif;
        }

    public static function AddNews($Array)
        {
        if (!StorageHabbo::$NeedDatabase):
            StorageHabbo::$NeedDatabase = true;
        endif;
        if (!StorageHabbo::CanDatabase(1)):
            echo xEcho("Exception on Violation Database: function AddNews");
        else:
            $Title                 = htmlentities(addslashes($Array['title']));
            $Image                 = htmlentities(addslashes($Array['image']));
            $Text                  = htmlentities(addslashes($Array['text']));
            Adapter::Query("INSERT INTO cms_news (title,image,text) VALUES ('$Title','$Image','$Text');");
            $_POST['HotelMessage'] = array('Success' => 'Article Created Succefully.. Reenter on Hotel to See Changes.');
        endif;
        }

    public static function RemoveNews($ArticleId)
        {
        if (!is_numeric($ArticleId)):
            echo xEcho("Fucked Idiot Hacker Go to Hell");
        else:
            if (StorageHabbo::getHabbo('IsAdmin') == true):
                if (!StorageHabbo::$NeedDatabase):
                    StorageHabbo::$NeedDatabase = true;
                endif;
                if (!StorageHabbo::CanDatabase(1)):
                    echo xEcho("Exception on Violation Database: function RemoveNews");
                else:
                    Adapter::Query("DELETE FROM cms_news WHERE id = $ArticleId;");
                    $_POST['HotelMessage'] = array('Success' => 'Article Deleted Succefully.. Reenter on Hotel to See Changes.');
                endif;
            else:
                $_POST['HotelMessage'] = array('Error' => 'You Are Not Administrator!');
            endif;
        endif;
        }

    /* End Section */

    /* Section Promos from Index */

    public static function Promos($Return = 1)
        {
        if (!StorageHabbo::PromosExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (StorageHabbo::CanDatabase(1)):
                $Query = Adapter::Query("SELECT username,motto,look FROM users ORDER BY RAND()");
                $Nums  = Adapter::Counts($Query);
                $Query = Adapter::Query("SELECT username,motto,look FROM users ORDER BY RAND() LIMIT $Nums");
                $Id    = 1;
                while ($Row   = Adapter::FetchAll($Query))
                    {
                    $Array[$Id] = array('id' => $Id, 'username' => $Row['username'], 'motto' => $Row['motto'], 'look' => $Row['look']);
                    $Id++;
                    }
                $_SESSION['PromoData'] = $Array;
                $_SESSION['PromoNums'] = $Nums;
            else:
                echo xEcho("Exception on Violation Database: function Promos");
                $_SESSION['PromoData'] = '';
                $_SESSION['PromoNums'] = 0;
            endif;
            if ($Return):
                return $_SESSION['PromoData'];
            endif;
        else:
            if ($Return):
                return $_SESSION['PromoData'];
            endif;
        endif;
        }

    public static function PromosExists()
        {
        if (!empty($_SESSION['PromoData']) && $_SESSION['PromoData'] != ""):
            return true;
        else:
            return false;
        endif;
        }

    /* End Section */

    /* Section Staffs Page */

    public static function Staffs($Return = 1)
        {
        if (!StorageHabbo::StaffExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (StorageHabbo::CanDatabase(1)):
                $Code  = "";
                $Ranks = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => 'Moderators', 6 => 'Administrators', '7' => 'Managers', 8 => 'Directors', 9 => 'Owners', 10 => 'Creators');
                for ($R = 10; $R >= 5; $R--)
                    {
                    $Rank  = $Ranks[$R];
                    $Code .= "<div  class=\"cbb clearfix\" id=\"avatar-selector-habblet\">";
                    $Code .= "<h2 class=\"title\">$Rank</h2><ul>\n";
                    $Odd   = "even";
                    $Query = Adapter::Query("SELECT username,look,gender,motto FROM users WHERE rank = '$R'");
                    while ($Row   = Adapter::FetchAll($Query))
                        {
                        $Odd = ($Odd == "even") ? "odd" : "even";
                        $Code .= "<li class=\"$Odd\">\n";
                        $Code .= "<img class=\"avatar-image\" src=\"http://habbo.de/habbo-imaging/avatarimage?figure={$Row["look"]}&size=s&direction=4&head_direction=4&gesture=sml\" width=\"33\" height=\"56\"/>\n";
                        $Code .= "<div class=\"avatar-info\">\n";
                        $Code .= "<div class=\"avatar-info-container\">\n";
                        $Code .= "<div class=\"avatar-name\">{$Row["username"]}</div>\n";
                        $Code .= "<div class=\"avatar-info\">{$Row["motto"]}\n";
                        $Code .= "</div>\n";
                        $Code .= "</div>\n";
                        $Code .= "</div>\n";
                        $Code .= "</li>\n";
                        }
                    $Code .= "</ul></div>";
                    }
                $_SESSION['StaffData'] = $Code;
            else:
                echo xEcho("Exception on Violation Database: function Staffs");
                $_SESSION['StaffData'] = null;
            endif;
            if ($Return):
                return $_SESSION['StaffData'];
            endif;
        else:
            if ($Return):
                return $_SESSION['StaffData'];
            endif;
        endif;
        }

    public static function StaffExists()
        {
        if (!empty($_SESSION['StaffData']) && $_SESSION['StaffData'] != ""):
            return true;
        else:
            return false;
        endif;
        }

    /* End Section */

    /* Section Habbo Data and Hotel Data */

    public static function Habbo($Id)
        {
        if (!StorageHabbo::HabboExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (!StorageHabbo::CanDatabase(1)):
                echo xEcho("Exception on Violation Database: function Habbo");
            else:
                $Query = Adapter::Query("SELECT * FROM users WHERE id = $Id LIMIT 1");
                $Fetch = Adapter::FetchAll($Query);
                if ($Fetch['rank'] >= 7):
                    $IsAdmin = true;
                else:
                    $IsAdmin = false;
                endif;
                self::$Habbo           = new Habbo($Id, $Fetch['username'], $Fetch['mail'], $Fetch['gender'], $Fetch['motto'], $Fetch['credits'], $Fetch['activity_points'], "127.0.0.1", "Default", $Fetch['look'], $IsAdmin);
                $_SESSION['HabboData'] = serialize(self::$Habbo);
            endif;
        endif;
        }

    public static function Hotel($IfReturn = 0)
        {
        if (!StorageHabbo::HotelExists()):
            if (!StorageHabbo::$NeedDatabase):
                StorageHabbo::$NeedDatabase = true;
            endif;
            if (!StorageHabbo::CanDatabase(1)):
            else:
                $QuerA                 = Adapter::Query("SELECT * FROM server_status");
                $FetcA                 = Adapter::FetchAll($QuerA);
                $QuerB                 = Adapter::Query("SELECT id FROM users");
                $NumeB                 = Adapter::Counts($QuerB);
                if ($FetcA['users_online'] == 0)
                    $FetcA['users_online'] = "None";
                if ($NumeB == 0)
                    $NumeB                 = "None";
                self::$Hotel           = new Hotel($FetcA['users_online'], $FetcA['server_ver'], $NumeB);
                $_SESSION['HotelData'] = serialize(self::$Hotel);
                if ($IfReturn == 1):
                    return array('online_users' => $FetcA['users_online'], 'registered_users' => $NumeB);
                endif;
            endif;
        endif;
        }

    public static function HabboExists()
        {
        if (!empty($_SESSION['HabboData']) && $_SESSION['HabboData'] != ""):
            self::$Habbo = unserialize($_SESSION['HabboData']);
            return true;
        else:
            return false;
        endif;
        }

    public static function HotelExists()
        {
        if (!empty($_SESSION['HotelData'])):
            self::$Hotel = unserialize($_SESSION['HotelData']);
            return true;
        else:
            return false;
        endif;
        }

    /* End Section */

    /* Section GetData */

    public static function getHabbo($Var = "none")
        {
        if (StorageHabbo::HabboExists()):
            if ($Var == "none"):
                $Vars = get_class_vars(get_class(self::$Habbo));
                foreach ($Vars as $Name => $Value)
                    {
                    $Value             = self::$Habbo->getHabbo($Name);
                    $HabboArray[$Name] = $Value;
                    }
                return $HabboArray;
            else:
                return self::$Habbo->getHabbo($Var);
            endif;
        else:
            return "not_logged";
        endif;
        }

    public static function getHotel($Var = "none")
        {
        if (StorageHabbo::HotelExists()):
            if ($Var == "none"):
                $Vars = get_class_vars(get_class(self::$Hotel));
                foreach ($Vars as $Name => $Value)
                    {
                    $Value             = self::$Hotel->getHotel($Name);
                    $HotelArray[$Name] = $Value;
                    }
                return $HotelArray;
            else:
                return self::$Hotel->getHotel($Var);
            endif;
        else:
            echo 'Hotel Not Started';
            return "not_logged";
        endif;
        }

    /* End Section */

    public static function Destroy()
        {
        self::$Habbo             = null;
        self::$Hotel             = null;
        self::$NeedDatabase      = true;
        session_start();
        $_SESSION['NeedRefresh'] = true;
        }

    public static function CanDatabase($IsIsset = 0)
        {
        if ($IsIsset == 1):
            if (Adapter::Get() != null):
                return true;
            else:
                return false;
            endif;
        else:
            if (self::$NeedDatabase == true):
                self::$NeedDatabase == false;
                return true;
            else:
                return false;
            endif;
        endif;
        }

    }

function xEcho($Text, $SpecialType = 'none')
    {
    switch ($SpecialType)
        {
        case 'none':
            $Type = 'p=index';
            break;
        case 'logout':
            $Type = 'a=logout';
            break;
        }
    $Code = "<style>#login-errors {width: 100%;height: auto;background-color: #fc6621;color: #fff;font-size: 17px;font-weight: bold;text-align: center;line-height: 35px;}</style>\n";
    $Code .= "<div id=\"login-errors\">\n";
    $Code .= $Text . "<a id=\"forgot-password-localization-link\" style=\"font-size:13pt;color:black;\" href=\"?$Type\"> Go Back.</a>" . "\n";
    $Code .= "</div>\n";
    return $Code;
    }

if (isset($_SESSION['NeedRefresh'])):
    header("Refresh:0");
    $_SESSION['NeedRefresh'] = null;
else:
    $Habbo = StorageHabbo::getHabbo();
    $Page  = new Page($Settings, $Hotel, $Habbo);
    $Paget = $Page->SetPage();
    $Make  = $Page->Make($Paget['E'], $Paget['D']);
    if (Page::SubHeaderStarted() == 'not_started'):
        Page::StartSubHeader($SubHeader);
    endif;
    $Database = $Page->Database($Settings);
    if ($Database):
        $Result = $Page->Serialize($Make);
        if (StorageHabbo::CanDatabase(1)):
            Adapter::Close();
        endif;
        echo $Result;
    else:
        if (StorageHabbo::CanDatabase(1)):
            Adapter::Close();
        endif;
    endif;

endif;
echo '<!-- Loaded in ' . (microtime(true) - Start) . ' seconds -->';
?>