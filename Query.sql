/*
Navicat MySQL Data Transfer

Source Server         : Jogo
Source Server Version : 50172
Source Host           : localhost:3306
Source Database       : mercury

Target Server Type    : MYSQL
Target Server Version : 50172
File Encoding         : 65001

Date: 2014-11-28 11:18:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_news
-- ----------------------------
DROP TABLE IF EXISTS `cms_news`;
CREATE TABLE `cms_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `text` text COLLATE latin1_general_ci,
  `image` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of cms_news
-- ----------------------------
INSERT INTO `cms_news` VALUES ('12', 'Hello', 'FUUUUD', 'http://1.bp.blogspot.com/-pNuKa4Zc1zw/Us7k9A7BKPI/AAAAAAAAFaA/Ey6vJVTDL2Y/s1600/BR_Promo_Eusebio.png');
